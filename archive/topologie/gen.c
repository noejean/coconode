/*
 *  RealGraphHiveGenerator.c
 *  cd Desktop/Work/01\ -\ LIG\ -\ Grenoble/08\ -\ WSNet/Generator/
 *  
 *
 *  Created by Bogdan Pavkovic on 8/23/10.
 *  Modified by Malisha Vucinic on 26/04/12
 *  Copyright 2010 ENSIMAG. All rights reserved.
 *
 */
//Compile:      gcc -o gridGen gridGenerator.c mem_fs.c das.c
//Excute:       ./gridGen -a 5 -b 5 -d 2.5 (-G 2)
//f: 2: #format SIMPLE -1: copy to some folder
//Visualise:    java -jar ~/Topogen/dist/Topogen.jar -s graph.data -z 6 -r 5
//Visualise:    java -jar ~/Topogen/dist/Topogen.jar -s graph.data -z 100

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <stdint.h>
#include "das.h"
#include "mem_fs.h"
#include "smarthomegraph_parameters.h"
#include "random.h"

typedef signed long int uint_t;

struct neighbor {
    uint_t id;
	double x;
	double y;
};

struct node {
	struct neighbor data;
	void* neighbors;
};

void init_home_plan();
void destroy_rooms();
void init_counters();
void place_sensors();
void place_actuators();
void place_switches();
void destroy ();
void place_controller();

struct room {
	unsigned int type;
	unsigned int id;

	// Dimensions in meters
	double x_length;
	double y_length;

	// Sensor nodes counters
	long unsigned int sensor_counter;
	long unsigned int actuator_counter;
	long unsigned int switch_counter;

	// Coordinates of the lower left corner
	double x_coordinate;
	double y_coordinate;
	double z_coordinate;

	// Density coefficient [-3,3] with mean 0. 0 is the typical density.
	unsigned int density_coeff;
};
void place_device(char *, struct room *);
long *seed;
long total_placed_devices = 0;

void init_density(struct room *);

FILE * pFile;

void *list_of_nodes=NULL; // List of all nodes
void *list_of_rooms=NULL; // List of all rooms in a home

struct node *node;
struct room *room;
struct neighbor *neighbor;

double x,y;
//long int nodeID;

unsigned int N          = -1;// Init number of nodes to be generated
uint_t l          = -1;// Init dimension of the generated field
signed int f	  = 2;// Init either to show graph or just to copy file
long unsigned int NodeId = 0; 
double total_area = 0;

unsigned int percentage_sensors = -1;
unsigned int percentage_actuators = -1;
long int dist		= -1;// Init distance between 2 nodes in the grid
long int dens		= 1;// Init num additional nodes between 2 pilon nodes in grid

char* usage_str="Error: Entry should be in the following form: -N 'Number of nodes' -s 'Percentage of sensors [int]' -a 'Percentage of actuators [int]'\n'";

void usage(void){
	fprintf(stderr, "%s", usage_str);
	return;
}


int create_Node(long unsigned int id, char* type, float x, float y, float z){
	node = (struct node *) malloc(sizeof(struct node));
	node->data.id = id;
	node->data.x  = x;
	node->data.y  = y;
	
	node->neighbors = das_create();
	
	//printf("Node = %ld coord = (%lf, %lf)\n", id, x, y);

	das_insert(list_of_nodes, (void *) node);

	return 0;
}


void *generate_nodes (int number_of_sensors, int percentage_sensor, 
		int percentage_actuator){
	N = number_of_sensors;
	percentage_actuators = percentage_actuator;
	percentage_sensors = percentage_sensor;
	l = -1;
	f  = 2;
	NodeId = 0; 
	total_area = 0;
	dist= -1;
	dens= 1;
	
	
	if (N > 0 && (percentage_sensors + percentage_actuators == 100)) {
		destroy ();
		//Flags&counters
		short int accepted,flag;
		long int i,j,count;
		
		// Initializing the random number generator!
		seed = (long *) malloc(sizeof(long));
		*seed = time(NULL);
		for(i=0;i<100;i++)randint(0,MODULE-1,seed);
		// end initialization of the random number generator

		//printf("seed = %ld\n",seed);
							
		das_init();
		
		init_home_plan();
		
		init_counters();


		list_of_nodes = das_create();//Create List of all nodes
		place_controller();
		place_sensors();
		place_actuators();
		place_switches();		

// 		//Destroy Lista
// 		while ((node = (struct node *) das_traverse(list_of_nodes)) != NULL) {
// 			das_destroy(node->neighbors);
// 		}
// 		
// 		das_destroy(list_of_nodes);
// 		destroy_rooms();
// 		
// 		//Close file
// 		fclose (pFile);
		return list_of_nodes;
	}
	else {
		usage();
		return NULL;
	}

	printf("TOTAL NUMBER OF NODES IN THE GRAPH: %ld\n",total_placed_devices);	
	return 0; 
}

void destroy (){
	while ((node = (struct node *) das_traverse(list_of_nodes)) != NULL) {
		das_destroy(node->neighbors);
	}
	das_destroy(list_of_nodes);
	destroy_rooms();
}

void init_home_plan(){
int i = 0;
list_of_rooms = das_create();

for(i = 0; i < NUMBER_OF_ROOMS; i++)
{
	room = (struct room *) malloc(sizeof(struct room));

	room -> id = i;

	switch (room->id) {
			case 0:
				room -> type = BEDROOM;

				room -> x_length = 4;
				room -> y_length = 3;

				room -> x_coordinate = 0;
				room -> y_coordinate = 0; 
				room -> z_coordinate = 0;

				break;

			case 1:
				room -> type = BATHROOM;

				room -> x_length = 4;
				room -> y_length = 2;

				room -> x_coordinate = 0;
				room -> y_coordinate = 3 + WALL_THICKNESS; 
				room -> z_coordinate = 0;

				break;

			case 2:
				room -> type = BEDROOM;

				room -> x_length = 4;
				room -> y_length = 3;

				room -> x_coordinate = 0;
				room -> y_coordinate = 5 + WALL_THICKNESS; 
				room -> z_coordinate = 0;

				break;

			case 3:
				room -> type = OFFICE;

				room -> x_length = 4;
				room -> y_length = 2;

				room -> x_coordinate = 0;
				room -> y_coordinate = 8 + WALL_THICKNESS; 
				room -> z_coordinate = 0;

				break;

			case 4:
				room -> type = HALL;

				room -> x_length = 3;
				room -> y_length = 3;

				room -> x_coordinate = 4 + WALL_THICKNESS;
				room -> y_coordinate = 0; 
				room -> z_coordinate = 0;

				break;

			case 5:
				room -> type = LIVING_ROOM;

				room -> x_length = 5;
				room -> y_length = 7;

				room -> x_coordinate = 4 + WALL_THICKNESS;
				room -> y_coordinate = 3 + WALL_THICKNESS; 
				room -> z_coordinate = 0;

				break;

			case 6:
				room -> type = KITCHEN;

				room -> x_length = 6;
				room -> y_length = 3;

				room -> x_coordinate = 7 + WALL_THICKNESS;
				room -> y_coordinate = 0; 
				room -> z_coordinate = 0;

				break;

			case 7:
				room -> type = DINING_ROOM;

				room -> x_length = 4;
				room -> y_length = 2;

				room -> x_coordinate = 9 + WALL_THICKNESS;
				room -> y_coordinate = 3 + WALL_THICKNESS; 
				room -> z_coordinate = 0;

				break;

			case 8:
				room -> type = BEDROOM;

				room -> x_length = 4;
				room -> y_length = 3;

				room -> x_coordinate = 9 + WALL_THICKNESS;
				room -> y_coordinate = 5 + WALL_THICKNESS; 
				room -> z_coordinate = 0;

				break;

			case 9:
				room -> type = BATHROOM;

				room -> x_length = 4;
				room -> y_length = 2;

				room -> x_coordinate = 9 + WALL_THICKNESS;
				room -> y_coordinate = 8 + WALL_THICKNESS; 
				room -> z_coordinate = 0;

				break;

			case 10:
				room -> type = BALCONY;

				room -> x_length = 5;
				room -> y_length = 1.5;

				room -> x_coordinate = 4 + WALL_THICKNESS;;
				room -> y_coordinate = 10 + WALL_THICKNESS; 
				room -> z_coordinate = 0;

				break;
			default:
				printf("Parameters not set well!\n");
		}
	total_area += room->x_length * room->y_length;

	room -> sensor_counter = 0;
	room -> actuator_counter = 0;
	room -> switch_counter = 0;

	init_density(room);

	das_insert(list_of_rooms, (void *) room);

}
printf("total area = %f\n",total_area);

}

void init_counters(){
int total_sensors = 0;
int total_actuators = 0;
int total_switches = 0;
int switches;
int  i = 0, overflow = 0, rand_num;
	while ((room = (struct room *) das_traverse(list_of_rooms)) != NULL) {

			room -> sensor_counter =  round(N * percentage_sensors/100 * (room->x_length * room->y_length)/total_area);
			room ->actuator_counter =  round(N * percentage_actuators/100 * (room->x_length*room->y_length)/total_area);
			
			switches = round( RATIO_SWITCHES * room->sensor_counter);
			room->switch_counter = switches;
			room->sensor_counter -= switches;
			
			

			//printf("ROOM ID: %ld; sensor_counter = %ld; actuator_counter = %ld\n",(long int) room->id,(long int) room->sensor_counter,(long int) room->actuator_counter);
			
			total_sensors += room->sensor_counter;
			total_switches += room->switch_counter;
			total_actuators += room->actuator_counter;						
		}

//printf("total sensors before check = %d\n",total_sensors);
//printf("total switches before check = %d\n", total_switches);
//printf("total actuators before check = %d\n",total_actuators);

if(total_sensors + total_actuators + total_switches != N){
	if((total_sensors + total_switches) > round((float)N*percentage_sensors/100))
	{	
		overflow = (total_sensors + total_switches) - round((float)N * percentage_sensors/100);
		for(i = 0; i < overflow; i++){

			rand_num = randint(0, NUMBER_OF_ROOMS - 1, seed);

			while ((room = (struct room *) das_traverse(list_of_rooms)) != NULL)
			{
				if(room -> id == rand_num)
				{
						room ->sensor_counter--;
						total_sensors--;
						printf("Decrementing SENSORS count in room %d\n",room->id);
				}	
			}
		}

	}
	else if((total_sensors + total_switches) < round ( (float)N * percentage_sensors/100))
	{

		overflow =  round((float)N * percentage_sensors/100) - (total_sensors + total_switches);
			for(i = 0; i < overflow; i++){

			rand_num = randint(0, NUMBER_OF_ROOMS - 1, seed);

			while ((room = (struct room *) das_traverse(list_of_rooms)) != NULL)
			{
				if(room -> id == rand_num)
				{
						room ->sensor_counter++;
						total_sensors++;
						printf("Incrementing SENSORS count in room %d\n",room->id);
				}	
			}
		}
	}
	if(total_actuators > round((float)N*percentage_actuators/100))
	{
		overflow =  total_actuators - round((float)N * percentage_actuators/100);
		for(i = 0; i < overflow; i++){

			rand_num = randint(0, NUMBER_OF_ROOMS - 1, seed);


			while ((room = (struct room *) das_traverse(list_of_rooms)) != NULL)
			{
				if(room -> id == rand_num){
						room ->actuator_counter--; 
						total_actuators--;
						printf("Decrementing ACTUATORS count in room %d\n",room->id);
				}
			}	
		}
	}
	else if(total_actuators < round ( (float) N * percentage_actuators/100))
	{
		overflow =  round((float)N * percentage_actuators/100) - total_actuators;
			for(i = 0; i < overflow; i++){

			rand_num = randint(0, NUMBER_OF_ROOMS - 1, seed);

			while ((room = (struct room *) das_traverse(list_of_rooms)) != NULL)
			{
				if(room -> id == rand_num)
				{
						room ->actuator_counter++;
						total_actuators++;
						printf("Incrementing SENSORS count in room %d\n",room->id);
				}	
			}
		}
	}

	

}

printf("TOTAL NUMBER OF SENSORS = %d\nTOTAL NUMBER OF ACTUATORS = %d\nTOTAL NUMBER OF SWITCHES = %d\nTOTAL= %d plus the controler\n",total_sensors,total_actuators,total_switches,total_sensors+total_actuators+total_switches);

}

void destroy_rooms(){
//Destroy Lista
		while ((room = (struct room *) das_traverse(list_of_rooms)) != NULL) {

			//printf("ROOM: ID %d, origin (%f,%f), xlength = %f, ylength = %f, density coeff = %d\n",room->id, room->x_coordinate, room->y_coordinate, room->x_length, room->y_length, room->density_coeff);
			//das_destroy(room);								
		}
		//printf("TOTAL_DENSITY_COEFF = %d\n",TOTAL_DENSITY_COEFF);
		das_destroy(list_of_rooms);


}


void init_density(struct room *room){

	switch(room -> type){
			case BEDROOM:
				room->density_coeff = BEDROOM_DENSITY;
				break;

			case BATHROOM:
				room -> density_coeff = BATHROOM_DENSITY;
				break;
			
			case OFFICE:
				room -> density_coeff = OFFICE_DENSITY;
				break;
			
			case HALL:
				room -> density_coeff = HALL_DENSITY;
				break;

			case LIVING_ROOM:
				room -> density_coeff = LIVING_ROOM_DENSITY;
				break;
			
			case KITCHEN:
				room -> density_coeff = KITCHEN_DENSITY;
				break;
			
			case DINING_ROOM:
				room -> density_coeff = DINING_ROOM_DENSITY;
				break;

			case BALCONY:
				room -> density_coeff = BALCONY_DENSITY;
				break;
			default:
				printf("error in logic!\n");
		}
// room ->density_coeff = room->density_coeff / TOTAL_DENSITY_COEFF;
}

void place_device(char *type, struct room *room){
	int i = 0, side = 0;
	float height, width;
	float p;

	float x_coordinate = 0, y_coordinate = 0, z_coordinate = 0;

			if((p = uniform(0,1,seed)) < WALL_PROBABILITY) // place a device on walls (1-4) or the ceiling (5)
			{
				side = randint(1, 5, seed);
				height = uniform(0, CEILING_HEIGHT, seed);
				switch(side){
					case 1: //wall 1
						x_coordinate = uniform (0, room->x_length, seed);
						y_coordinate = 0;
						z_coordinate = height;
					break;

					case 2: // wall 2
											
						x_coordinate = room -> x_length;
						y_coordinate = uniform (0, room->y_length, seed);
						z_coordinate = height;
					break;

					case 3: // wall 3
						x_coordinate = uniform (0, room->x_length, seed);
						y_coordinate = room->y_length;
						z_coordinate = height;
					break;

					case 4: // wall 4
						x_coordinate = 0;
						y_coordinate = uniform (0, room->y_length, seed);
						z_coordinate = height;
					break;

					case 5: // ceiling
						x_coordinate = uniform(0,room->x_length,seed);
						y_coordinate = uniform (0, room->y_length, seed);
						z_coordinate = CEILING_HEIGHT;	 // Deterministic!
						
					break;
				}
			}
			else // place a device around the room (a home appliance)
			{

				x_coordinate = uniform(0, room -> x_length, seed);
				y_coordinate = uniform (0, room -> y_length, seed);
				z_coordinate = uniform(0, CEILING_HEIGHT/3, seed);

			}

	create_Node(NodeId++,type, (float) x_coordinate + room->x_coordinate, (float) y_coordinate + room->y_coordinate, (float)z_coordinate + room->z_coordinate );
	total_placed_devices++;
		
}

void place_sensors(){
	int i = 0;

	while ((room = (struct room *) das_traverse(list_of_rooms)) != NULL){
		for(i = 0; i < room->sensor_counter; i++)
		{
			place_device("sensor",room);
		}
	}

}

void place_actuators(){
	int i = 0;
	
	while ((room = (struct room *) das_traverse(list_of_rooms)) != NULL){
		for(i = 0; i < room->actuator_counter; i++)
		{
			place_device("actuator", room);
		}
	}


}

void place_switches(){
int i = 0;

	while ((room = (struct room *) das_traverse(list_of_rooms)) != NULL){
		for(i = 0; i < room->switch_counter; i++)
		{
			place_device("wallswitch",room);
		}
	}


}

void place_controller(){
	while ((room = (struct room *) das_traverse(list_of_rooms)) != NULL)
	{
		if(room->type == LIVING_ROOM)
		{
			place_device("controller", room);
			//return;
		}
	}

}
