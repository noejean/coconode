#ifndef __PLOTTER_H__
#define __PLOTTER_H__

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdarg.h>


void vgenerate_ggraph_hist(const char *graph_type, const char *datain_filename, 
	const char *dataout_type, const char *dataout_filename, const char *title, 
	const char *xlabel, const char *ylabel, int xcolumn, int ycolumn, 
	int bitmap_width, int bitmap_height);

void vgenerate_ggraph(const char *graph_type, const char *datain_filename, 
	const char *dataout_type, const char *dataout_filename, const char *title, 
	const char *xlabel, const char *ylabel, int xcolumn, int ycolumn, 
	int bitmap_width, int bitmap_height);

void generate_ggraph(const char* graph_type, const char* datain_filename, 
	const char* dataout_type, const char* dataout_filename, 
	const char* title, ...);


//void generate_ggraph(const char *graph_type, const char *datain_filename, 
//	const char *dataout_type, const char *dataout_filename, const char *title
//	const char *xlabel, const char *ylabel, int xcolumn, int ycolumn, 
//	int bitmap_width, int bitmap_height);

void init_gnuplot(const char *pngout_filename, int pngout_width, int pngout_height);

char** list_files(const char* folder, const char* filter);

void pdf(const char *datain_filename, int column);
void pdff();
/*void compute_mean_gnuplot(const char *datain_filename);*/


#endif

