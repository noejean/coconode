#include "simulation.h"
#define LONG 100

#define COOJA_JAR_PATH "/tools/cooja/dist/cooja.jar"
#define LOG4J_JAR_PATH "/tools/cooja/dist/lib/log4j.jar"
#define JDOM_JAR_PATH "/tools/cooja/dist/lib/jdom.jar"

extern int is_cooja_running;

jobject sim = NULL;
jmethodID StartSimulation;
jmethodID isRunning;
jclass GUIclass;
jclass class_File;
jclass class_URL;
jclass class_Simulation;
jmethodID mid_getSimulationTimeMillis;
jmethodID stopSimulation;
jmethodID mid_doRemoveSimulation;
 jfieldID fidFrame;
 jobject GUI;
JavaVM*  jvm;      /* denotes a Java VM */
JNIEnv* env;      /* pointer to native method interface */


int init_jvm (){
  JavaVMInitArgs     vm_args;
    JavaVMOption     options[10];
    int oi = 0;
    
    if(getenv("JAVA_HOME") == NULL || strcmp(getenv("JAVA_HOME"),"")==0){
      printf("JAVA_HOME PAS BON %s\n",getenv("JAVA_HOME"));
    }
    options[oi].optionString = malloc((16+strlen(getenv("JAVA_HOME"))+1)*sizeof(char));
    strcpy(options[oi].optionString,"");
    strcat(options[oi].optionString,"-Djava.compiler=");
    strcat(options[oi++].optionString,getenv("JAVA_HOME"));
//     options[oi++].optionString = "-Djava.compiler=/home/florian/Bureau/Malisha-code/java-6-sun";
    
    /**
     * A changer, je pense qu'en static ca passe
     */
     options[oi++].optionString = "-Djava.class.path=./src/controller/class/loader.jar";
   
//     options[oi].optionString = malloc((18+strlen(contiki_path) + strlen(COOJA_JAR_PATH) + 1
// 				  +strlen(contiki_path)+strlen(LOG4J_JAR_PATH) +1 
// 				  +strlen(contiki_path)+strlen(JDOM_JAR_PATH)+1)*sizeof(char));
//     strcpy(options[oi].optionString,"");
//     strcat(options[oi].optionString,"-Djava.class.path=");
//     strcat(options[oi].optionString,contiki_path);
//     strcat(options[oi].optionString,COOJA_JAR_PATH);
//     strcat(options[oi].optionString,":");
//     strcat(options[oi].optionString,contiki_path);
//     strcat(options[oi].optionString,LOG4J_JAR_PATH);
//     strcat(options[oi].optionString,":");
//     strcat(options[oi].optionString,contiki_path);
//     strcat(options[oi++].optionString,JDOM_JAR_PATH);

//     options[oi++].optionString = "-Djava.class.path=/home/flo/Bureau/Malisha-code/Malisha-code/ctk-load/tools/cooja/dist/cooja.jar:/home/flo/Bureau/Malisha-code/Malisha-code/ctk-load/tools/cooja/dist/lib/log4j.jar:/home/flo/Bureau/Malisha-code/Malisha-code/ctk-load/tools/cooja/dist/lib/jdom.jar";
//     :/System/Library/Frameworks/JavaVM.framework/Classes/classes.jar";
    vm_args.version = JNI_VERSION_1_6;
    vm_args.options = options;
    vm_args.nOptions = oi;
    vm_args.ignoreUnrecognized = 0;  
    long result = JNI_CreateJavaVM( &jvm, (void**)&env, &vm_args );
    if(result == JNI_ERR ) 
    {
      printf("Error invoking the JVM\n");
      return (-1);
    }
  return 1;

}

int simulation (char *csc_config_file, char *contiki_path) {
    
//     JavaVMInitArgs     vm_args;
//     JavaVMOption     options[10];
    char log_config_path [100] ="/";
    // init_jvm ();
//     if(getenv("JAVA_HOME") == NULL || strcmp(getenv("JAVA_HOME"),"")==0){
//       printf("JAVA_HOME PAS BON %s\n",getenv("JAVA_HOME"));
//     }
//     options[oi].optionString = malloc((16+strlen(getenv("JAVA_HOME"))+1)*sizeof(char));
//     strcpy(options[oi].optionString,"");
//     strcat(options[oi].optionString,"-Djava.compiler=");
//     strcat(options[oi++].optionString,getenv("JAVA_HOME"));
// //     options[oi++].optionString = "-Djava.compiler=/home/florian/Bureau/Malisha-code/java-6-sun";
    
//     /**
//      * A changer, je pense qu'en static ca passe
//      */
//     options[oi++].optionString = "-Djava.class.path=/home/quan/build/coconode/coconode/src/class/loader.jar";
   
// //     options[oi].optionString = malloc((18+strlen(contiki_path) + strlen(COOJA_JAR_PATH) + 1
// // 				  +strlen(contiki_path)+strlen(LOG4J_JAR_PATH) +1 
// // 				  +strlen(contiki_path)+strlen(JDOM_JAR_PATH)+1)*sizeof(char));
// //     strcpy(options[oi].optionString,"");
// //     strcat(options[oi].optionString,"-Djava.class.path=");
// //     strcat(options[oi].optionString,contiki_path);
// //     strcat(options[oi].optionString,COOJA_JAR_PATH);
// //     strcat(options[oi].optionString,":");
// //     strcat(options[oi].optionString,contiki_path);
// //     strcat(options[oi].optionString,LOG4J_JAR_PATH);
// //     strcat(options[oi].optionString,":");
// //     strcat(options[oi].optionString,contiki_path);
// //     strcat(options[oi++].optionString,JDOM_JAR_PATH);
//     printf("%s \n %s \n %d \n",options[0].optionString,options[1].optionString,oi);
// //     options[oi++].optionString = "-Djava.class.path=/home/flo/Bureau/Malisha-code/Malisha-code/ctk-load/tools/cooja/dist/cooja.jar:/home/flo/Bureau/Malisha-code/Malisha-code/ctk-load/tools/cooja/dist/lib/log4j.jar:/home/flo/Bureau/Malisha-code/Malisha-code/ctk-load/tools/cooja/dist/lib/jdom.jar";
// //     :/System/Library/Frameworks/JavaVM.framework/Classes/classes.jar";
//     vm_args.version = JNI_VERSION_1_6;
//     vm_args.options = options;
//     vm_args.nOptions = oi;
//     vm_args.ignoreUnrecognized = 0;  
//     long result = JNI_CreateJavaVM( &jvm, (void**)&env, &vm_args );
//     if(result == JNI_ERR ) 
//     {
//       printf("Error invoking the JVM\n");
//       //return 0;
//     }
    
    // jclass class_ClassLoader = (*env)->FindClass(env, "java/lang/ClassLoader");
    // jclass class_Class = (*env)->FindClass(env, "java/lang/Class");
    // jmethodID mid_getSystemClassLoader = (*env)->GetStaticMethodID( env, class_ClassLoader,
				// 	    "getSystemClassLoader", "()Ljava/lang/ClassLoader;");
    // jobject systemClassLoader = (*env)->CallObjectMethod(env,class_ClassLoader,mid_getSystemClassLoader,NULL);
    
    
    
    jclass Loaderclass = (*env)->FindClass( env, "Loader");
    // if(Loaderclass){
    //   printf("YES\n");
    // }
    // else{
    //   printf("NO\n");
    // }
    
//     options[oi].optionString = malloc((18+strlen(contiki_path) + strlen(COOJA_JAR_PATH) + 1
// 				  +strlen(contiki_path)+strlen(LOG4J_JAR_PATH) +1 
// 				  +strlen(contiki_path)+strlen(JDOM_JAR_PATH)+1)*sizeof(char));
//     strcpy(options[oi].optionString,"");
//     strcat(options[oi].optionString,"-Djava.class.path=");
//     strcat(options[oi].optionString,contiki_path);
//     strcat(options[oi].optionString,COOJA_JAR_PATH);
//     strcat(options[oi].optionString,":");
//     strcat(options[oi].optionString,contiki_path);
//     strcat(options[oi].optionString,LOG4J_JAR_PATH);
//     strcat(options[oi].optionString,":");
//     strcat(options[oi].optionString,contiki_path);
//     strcat(options[oi++].optionString,JDOM_JAR_PATH);
    
    
    jmethodID midLoader= (*env)->GetMethodID(env, Loaderclass, "<init>", "()V");
    jobject Loader = (*env)->NewObject(env, Loaderclass, midLoader);
    // if(Loader){
    //   printf("YES\n");
    // }
    // else{
    //   printf("NO\n");
    // }
    char * cooja_jar = malloc((strlen(contiki_path) + strlen(COOJA_JAR_PATH) + 1)*sizeof(char));
    strcpy(cooja_jar,"");
    strcat(cooja_jar,contiki_path);
    strcat(cooja_jar,COOJA_JAR_PATH);
    char * log4j_jar = malloc((strlen(contiki_path) + strlen(LOG4J_JAR_PATH) + 1)*sizeof(char));
    strcpy(log4j_jar,"");
    strcat(log4j_jar,contiki_path);
    strcat(log4j_jar,LOG4J_JAR_PATH);
    char * jdom_jar = malloc((strlen(contiki_path) + strlen(JDOM_JAR_PATH) + 1)*sizeof(char));
    strcpy(jdom_jar,"");
    strcat(jdom_jar,contiki_path);
    strcat(jdom_jar,JDOM_JAR_PATH);
    
    // printf("%s \n %s \n %s \n",cooja_jar,log4j_jar,jdom_jar);
    jstring chaine = (*env)->NewStringUTF(env, cooja_jar);
    jmethodID mid_addURL= (*env)->GetMethodID(env, Loaderclass, "addURL", "(Ljava/lang/String;)V");
    (*env)->CallObjectMethod(env,Loader, mid_addURL,chaine);
    chaine = (*env)->NewStringUTF(env, log4j_jar);
    (*env)->CallObjectMethod(env,Loader, mid_addURL,chaine);
    chaine = (*env)->NewStringUTF(env, jdom_jar);
    (*env)->CallObjectMethod(env,Loader, mid_addURL,chaine);
    jclass GUIclass = (*env)->FindClass( env, "se/sics/cooja/GUI");
    // if(GUIclass){
    //   printf("YES\n");
    // }
    // else{
    //   printf("NO\n");
    // }
    jclass class_File = (*env)->FindClass( env, "java/io/File");
    jclass class_Simulation = (*env)->FindClass(env, "se/sics/cooja/Simulation");
    jclass class_ScriptRunner = (*env)->FindClass(env, "se/sics/cooja/plugins/ScriptRunner");
    jclass class_DOMConfigurator = (*env)->FindClass(env, "org/apache/log4j/xml/DOMConfigurator");
    // jclass class_LogScriptEngine = (*env)->FindClass(env, "se/sics/cooja/plugins/LogScriptEngine");    
    
    
    jfieldID fid_externalToolsUserSettingsFile = (*env)->GetStaticFieldID(env, GUIclass, "externalToolsUserSettingsFile", "Ljava/io/File;");
    
    jmethodID mid_FILE_Constr = (*env)->GetMethodID(env, class_File, "<init>", "(Ljava/io/File;Ljava/lang/String;)V");
    
    jmethodID mid_FILE_Constr_from_string = (*env)->GetMethodID(env, class_File, "<init>", "(Ljava/lang/String;)V");
    
    jstring user_home = (*env)->NewStringUTF(env, getenv("HOME"));
   
    jobject file = (*env)->NewObject(env, class_File, mid_FILE_Constr_from_string, user_home);
    
    jfieldID fid_EXTERNAL_TOOLS_USER_SETTINGS_FILENAME = (*env)->GetStaticFieldID(env, GUIclass, "EXTERNAL_TOOLS_USER_SETTINGS_FILENAME", "Ljava/lang/String;");
    
    jstring str_EXTERNAL_TOOLS_USER_SETTINGS_FILENAME = (*env)->GetStaticObjectField(env,GUIclass,fid_EXTERNAL_TOOLS_USER_SETTINGS_FILENAME);
    
    jobject setting_file = (*env)->NewObject(env, class_File, mid_FILE_Constr, file,str_EXTERNAL_TOOLS_USER_SETTINGS_FILENAME);
    // if(setting_file){
    //   printf("YES \n");
    // }
    // else{
    //   printf("NO\n");
    // }
    (*env)->SetStaticObjectField(env, GUIclass, fid_externalToolsUserSettingsFile, setting_file);
   jfieldID fidContikiPath = (*env)->GetStaticFieldID(env, GUIclass, "specifiedContikiPath", "Ljava/lang/String;");
   jstring contiki_path_str = (*env)->NewStringUTF(env, contiki_path);
   (*env)->SetStaticObjectField(env, GUIclass, fidContikiPath, contiki_path_str);
   if ((*env)->ExceptionOccurred(env)) {
	(*env)->ExceptionDescribe(env);
    }
 
    jfieldID fid_LOG_CONFIG_FILE = (*env)->GetStaticFieldID(env, GUIclass, "LOG_CONFIG_FILE", "Ljava/lang/String;");
    // if(!fid_LOG_CONFIG_FILE){
    //   printf("YES fid log\n");
    // }
    // else{
    //   printf("NO fid log\n");
    // }
    if ((*env)->ExceptionOccurred(env)) {
	   (*env)->ExceptionDescribe(env);
    }
    jstring LOG_CONFIG_FILE = (*env)->GetStaticObjectField(env,GUIclass,fid_LOG_CONFIG_FILE);
    const char * log_config = (*env)->GetStringUTFChars(env,LOG_CONFIG_FILE,NULL);
    strcat(log_config_path,log_config);
    if(LOG_CONFIG_FILE){
      // printf("%s\n",log_config_path);
    }
    else{
      // printf("NO\n");
    }
    
    jmethodID getClass = (*env)->GetMethodID(env,GUIclass, "getClass", "()Ljava/lang/Class;" );
    jobject class_gui = (*env)->CallObjectMethod(env,GUIclass, getClass);
    jmethodID getResource = (*env)->GetMethodID( env,class_gui, "getResource", "(Ljava/lang/String;)Ljava/net/URL;" );
    jobject url = (*env)->CallObjectMethod(env,class_gui,getResource,(*env)->NewStringUTF(env,log_config_path));
    // if(url){
    //   printf("YES URL");
    // }
    // else{
    //   printf("NO\n");
    // }
    jmethodID mid_configure = (*env)->GetStaticMethodID( env, class_DOMConfigurator,"configure", "(Ljava/net/URL;)V");
    // if(!mid_configure){
    //   printf("YES configure\n");
    // }
    // else{
    //   printf("NO\n");
    // }

    (*env)->CallStaticVoidMethod(env,class_DOMConfigurator,mid_configure,url);
    if ((*env)->ExceptionOccurred(env)) {
    (*env)->ExceptionDescribe(env);
    }
 

   
    jmethodID midFILE = (*env)->GetMethodID(env, class_File, "<init>", "(Ljava/lang/String;)V");
    chaine = (*env)->NewStringUTF(env, csc_config_file);
    jobject objet = (*env)->NewObject(env, class_File, midFILE, chaine);

    // jmethodID mid_quickStartSimulationConfig = (*env)->GetStaticMethodID( env, GUIclass,"quickStartSimulationConfig", "(Ljava/io/File;Z)Lse/sics/cooja/Simulation;");
    // if(!mid_quickStartSimulationConfig){
    //   printf("YES mid SIM\n");
    // }
    // else{
    //   printf("NO mid SIM\n");
    // }


    mid_doRemoveSimulation = (*env)->GetMethodID(env, GUIclass, "doRemoveSimulation", "(Z)Z");
    
    jmethodID mid_createDesktopPane = (*env)->GetStaticMethodID( env, GUIclass,"createDesktopPane", "()Ljavax/swing/JDesktopPane;");

    jobject desktop = (*env)->CallStaticObjectMethod (env, GUIclass, mid_createDesktopPane);
    // if(desktop){
    //   printf("YES desktop\n");
    // }
    // else{
    //   printf("NO desktop\n");
    // }
     jmethodID midGUI = (*env)->GetMethodID(env, GUIclass, "<init>", "(Ljavax/swing/JDesktopPane;)V");
     GUI = (*env)->NewObject(env, GUIclass, midGUI, desktop);
     
    // if(GUI){
    //   printf("YES GUI\n");
    // }
    // else{
    //   printf("NO GUI\n");
    // }
     jclass class_JFrame = (*env)->FindClass(env, "javax/swing/JFrame");
     
    

//     jobject Frame = (*env)->GetObjectField(env, GUIclass, fidFrame);
//     if(Frame){
//       printf("YES Frame\n");
//     }
//     else{
//       printf("NO Frame\n");
//     }
//         jmethodID mid_setVisible = (*env)->GetMethodID(env, class_JFrame, "setVisible", "(Z)V");
//       (*env)->CallObjectMethod(env, Frame, mid_setVisible, JNI_FALSE);
    
    

//     jmethodID mid_configureFrame = (*env)->GetStaticMethodID( env, GUIclass,"configureFrame", "(Lse/sics/cooja/GUI;Z)V");
//     (*env)->CallStaticObjectMethod (env, GUIclass, mid_configureFrame, GUI, JNI_FALSE);
// //       gui.doQuit(true);
//         jmethodID mid_setVisible = (*env)->GetMethodID(env, class_JFrame, "setVisible", "(Z)V");
//       (*env)->CallObjectMethod(env, Frame, mid_setVisible, JNI_FALSE);
//       
     jmethodID mid_doLoadConfig = (*env)->GetMethodID(env, GUIclass, "doLoadConfig", "(ZZLjava/io/File;)V");
     (*env)->CallObjectMethod(env, GUI, mid_doLoadConfig, JNI_FALSE,JNI_TRUE,objet);
     
      jmethodID mid_getSimulation= (*env)->GetMethodID( env,GUIclass, "getSimulation", "()Lse/sics/cooja/Simulation;" );
      sim = (*env)->CallObjectMethod(env, GUI, mid_getSimulation);

      mid_getSimulationTimeMillis= (*env)->GetMethodID( env,class_Simulation, "getSimulationTimeMillis", "()J" );
/*     
     jmethodID mid_tryStartPlugin = (*env)->GetMethodID(env, 
				GUIclass, "tryStartPlugin", "(Ljava/lang/Class;Lse/sics/cooja/GUI;Lse/sics/cooja/Simulation;Lse/sics/cooja/Mote;)Lse/sics/cooja/Plugin;");
      if(!mid_tryStartPlugin){
	printf("YES truStart");
      }
      else{
	printf("NO trystart");
	
      }
     jobject plugin = (*env)->CallObjectMethod(env, GUI, mid_tryStartPlugin, class_ScriptRunner,GUI,sim, NULL);*/
     
//      jmethodID mid_setScriptActive = (*env)->GetMethodID(env, 
// 				class_ScriptRunner, "setScriptActive", "(Z)V");
//      (*env)->CallObjectMethod(env, plugin, mid_setScriptActive, JNI_TRUE);
     
//      ScriptRunner plugin = (ScriptRunner) gui.tryStartPlugin(ScriptRunner.class, gui, sim, null);
//           if (plugin == null) {
//             System.exit(1);
//           }
//           plugin.updateScript(scriptFile);
//           plugin.setScriptActive(true);
     
     
//     sim = (*env)->CallStaticObjectMethod (env, GUIclass, mid_quickStartSimulationConfig, objet, JNI_FALSE);
     
//       myFrame.setVisible(true)
// logger.info("> Starting COOJA");
    
//     JDesktopPane desktop = createDesktopPane();
//     if (vis) {
//       frame = new JFrame("COOJA Simulator");
// 	}
// 	GUI gui = new GUI(desktop);
// 	if (vis) {
//       configureFrame(gui, false);
//     }
// 
//     if (vis) {
//       gui.doLoadConfig(false, true, config);
//       return gui.getSimulation();



// LogScriptEngine engine 
// getPlugin(string)


    jmethodID mid_getPlugin= (*env)->GetMethodID( env,GUIclass, "getPlugin", "(Ljava/lang/String;)Lse/sics/cooja/Plugin;" );
    chaine = (*env)->NewStringUTF(env, "ScriptRunner");
    jobject plugin = (*env)->CallObjectMethod(env, GUI, mid_getPlugin,chaine);
    if(plugin){
      // printf("YES plugin\n");
    }
    else{
      // printf("NO plugin\n");
    }
    jfieldID fid_engine = (*env)->GetFieldID(env, class_ScriptRunner, "engine", "Lse/sics/cooja/plugins/LogScriptEngine;");
    jobject engine = (*env)->GetObjectField(env,plugin,fid_engine);
    if(engine){
      // printf("YES engine\n");
    }
    else{
      // printf("NO engine\n");
    }
    
    // jfieldID fid_semaphoreSim = (*env)->GetFieldID(env, class_LogScriptEngine, "semaphoreSim", "Ljava/util/concurrent/Semaphore;");
    // jobject semaphoreSim = (*env)->GetObjectField(env,engine,fid_semaphoreSim);
    
    StartSimulation= (*env)->GetMethodID( env,class_Simulation, "startSimulation", "()V" );
    (*env)->CallObjectMethod(env,sim,StartSimulation,NULL);
    isRunning= (*env)->GetMethodID( env,class_Simulation, "isRunning", "()Z" );
    int bool =  (int) ((*env)->CallObjectMethod(env,sim,isRunning));
    while(!bool){
        bool =  (int) ((*env)->CallObjectMethod(env,sim,isRunning));
    }


    jmethodID midJFrame = (*env)->GetMethodID(env, class_JFrame, "<init>", "(Ljava/lang/String;)V");
    chaine = (*env)->NewStringUTF(env, "Cooja sim");
    jobject JFrame = (*env)->NewObject(env, class_JFrame, midJFrame, chaine);

    fidFrame = (*env)->GetStaticFieldID(env, GUIclass, "frame", "Ljavax/swing/JFrame;");
    // if(!fidFrame){
    //   printf("YES fidFrame\n");
    // }
    // else{
    //   printf("NO fidFrame\n");
    // }
   (*env)->SetStaticObjectField(env, GUIclass, fidFrame,JFrame);
    // if(JFrame){
    //   printf("YES JFrame\n");
    // }
    // else{
    //   printf("NO JFrame\n");
    // }
    if(!sim){
      // printf("YES SIM\n");
    }
    else{
      // printf("NO\n");
      
//        isRunning()
      // jboolean bool = JNI_TRUE;
      // while (bool){
      //   bool = (*env)->CallObjectMethod(env,sim,isRunning);
      //   if(bool){
      //     printf("bool = TRUE\n");
      //     is_cooja_running = JNI_TRUE;
      //   }else{
      //     printf("bool = FALSE\n");
      //     is_cooja_running = JNI_FALSE;
      //   }
      // }
// 
//       }

       stopSimulation= (*env)->GetMethodID( env,class_Simulation, "stopSimulation", "()V" );
//  (*env)->CallObjectMethod(env,sim,stopSimulation,NULL);
	   
      return 1;
    }
    (*env)->DeleteLocalRef(env,objet);
    (*env)->DeleteLocalRef(env,setting_file);
    (*env)->DeleteLocalRef(env,file);
    if ((*env)->ExceptionOccurred(env)) {
	(*env)->ExceptionDescribe(env);
    }
    
//     (*jvm)->DestroyJavaVM( jvm );

    return 1;
}

long time_simulation () {
    return  (long) ((*env)->CallObjectMethod(env,sim,mid_getSimulationTimeMillis));
}

int pausing_simulation (){
  
  (*env)->CallObjectMethod(env,sim,stopSimulation,NULL);
//   (*env)->DeleteLocalRef(env,sim);
//   if ((*env)->ExceptionOccurred(env)) {
//     (*env)->ExceptionDescribe(env);
//   }
   // (*jvm)->DestroyJavaVM( jvm );
  return 1;
}

int stopping_simulation (){
  jobject null__obj = NULL;
  (*env)->CallObjectMethod(env,sim,stopSimulation,NULL);
  if(!mid_doRemoveSimulation){
    exit(-1);
  }
  (*env)->CallObjectMethod(env,GUI,mid_doRemoveSimulation,JNI_FALSE);
  (*env)->SetStaticObjectField(env, GUIclass, fidFrame,null__obj);
//   (*env)->DeleteLocalRef(env,sim);
//   if ((*env)->ExceptionOccurred(env)) {
//     (*env)->ExceptionDescribe(env);
//   }
//    (*jvm)->DestroyJavaVM( jvm );
  return 1;
}


int restart_simulation (){
  
  (*env)->CallObjectMethod(env,sim,StartSimulation,NULL);
//   (*env)->DeleteLocalRef(env,sim);
//   if ((*env)->ExceptionOccurred(env)) {
//     (*env)->ExceptionDescribe(env);
//   }
  return 1;
}

int check_simulation (){
  if(sim == NULL){
    return JNI_FALSE;
  }
  int bool =  (int) ((*env)->CallObjectMethod(env,sim,isRunning));
  return bool;
}