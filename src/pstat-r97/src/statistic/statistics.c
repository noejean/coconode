#include "../settings.h"
#include "statistics.h"


float mean_from_datain(const char *datain_filename, int column)
{
	FILE *datain;
	datain = fopen(datain_filename, "r");
	
	if(datain == NULL)
	{
		fprintf(stderr, "Cannot open %s. \n", datain_filename);
		exit(EXIT_FAILURE);
	}
	
	char *s=malloc(10000*sizeof(char));
	float value;
	float sum=0.0;
	float mean=0.0;
	int nb_indiv=0;
	
	int i;
	
	while (!feof(datain))
	{
		fgets(s, 9999, datain);

		i=1;
		while (i <= column)
		{
			fscanf(datain, "%f", &value);
			i++;
		}
		
		if (!feof(datain))
		{
			nb_indiv++;
			sum+=value;
		}
	}
	fclose(datain);
	
	mean=sum/(float) nb_indiv;
	
	return mean;
}



