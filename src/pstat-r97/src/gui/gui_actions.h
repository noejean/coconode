#ifndef __GUI_ACTIONS_H__
#define __GUI_ACTIONS_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include "../statistic/plotter.h"
#include "../statistic/statistics.h"

void quit(GtkWidget *widget, gpointer data);

void update_combo_oneindiv(GtkWidget *combo, gpointer widgs);

typedef struct
{
	/* widgets used to resize the gtkimage */
	GtkWidget *main_window;
	GtkWidget *graph_one_individual;
	GdkPixbuf *pixbuf_graph_one_individual;
	GtkWidget *graph_stat;
	GdkPixbuf *pixbuf_graph_stat;
	
	GtkWidget *hbox_global_settings_status;
	GtkWidget *hbox_one_individual_settings;
	GtkWidget *hbox_one_individual_export;
	GtkWidget *hbox_stat_settings;
	GtkWidget *hbox_stat_export;
	
	/* widgets used in the section generate_graph */
/*	GtkWidget *combo_one_individual;*/
	char* oneindiv_traced;
	GtkWidget *combo_stat_function;
	GtkWidget *combo_xaxis;
	GtkWidget *combo_yaxis;
/*	GtkWidget *graph_one_individual;*/
/*	GdkPixbuf *pixbuf_graph_one_individual;*/
/*	GtkWidget *graph_stat;*/
/*	GdkPixbuf *pixbuf_graph_stat;*/
} GenerateGraphWidgets;
void add_combo_box_entries_from_datain(GtkWidget *combo, const char *datain_filename);
void generate_graph(GtkWidget *widget, gpointer widgs);


void add_combo_box_entries_from_list_files(GtkWidget *combo);

typedef struct
{
	char* oneindiv_traced;
	GtkWidget* combo_one_individual;
	GenerateGraphWidgets* gene_graph_widgs;
} TraceOneIndivGraphWidgets;
void trace_oneindiv_graph(GtkWidget *widget, gpointer widgs);

typedef struct
{
	GtkWidget *combo_yaxis;
	GtkWidget *combo_stat_function;
	GenerateGraphWidgets* gene_graph_widgs;
} CompStatGraphWidgets;
void compute_stat_graph(GtkWidget *widget, gpointer widgs);



typedef struct
{
	GtkWidget *main_window;
	GtkWidget *combo_one_individual_format;
	GtkWidget *combo_one_individual;
	GtkWidget *combo_xaxis;
	GtkWidget *combo_yaxis;
} ExportGraphOneIndivWidgets;
void export_graph_one_indiv(GtkWidget *widget, gpointer widgs);

typedef struct
{
	GtkWidget* main_window; 
	GtkWidget* combo_stat_format;
	GtkWidget* combo_stat_function;
	GtkWidget *combo_xaxis;
	GtkWidget* combo_yaxis;
} ExportGraphStatWidgets;
void export_graph_stat(GtkWidget *widget, gpointer widgs);

void vexport_graph(GtkWidget* main_window, const char* graph_type, const char* datain_filename, 
	const char* dataout_type, const char *title, 
	const char *xlabel, const char *ylabel, int xcolumn, int ycolumn);

#endif

