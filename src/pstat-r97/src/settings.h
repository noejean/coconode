#ifndef __SETTINGS_H__
#define __SETTINGS_H__

#define MAIN_WINDOW_DEFAULT_WIDTH 2*640
#define MAIN_WINDOW_DEFAULT_HEIGHT 60+480+31

#define GNUPLOT_PATH "/usr/bin/gnuplot"

#define DATA_IN "./data/in/"
#define DATA_OUT "./data/out/"
#define DATA_DISPLAYED "./data/displayed"

#define FILENAME_FILTER ".dat"

#define DEFAULT_PNG_EXPORT_WIDTH "640"
#define DEFAULT_PNG_EXPORT_HEIGHT "480"

#endif


