#include "cercle.h"
#define WIDTH 500
#define HEIGHT 200
#define WIDTH_DRAW 455
#define HEIGHT_DRAW 140

GtkWidget *window;  //Declare une fentre GTK
GtkWidget *drawing_area; //Declare une zone de dessi
GtkWidget *vbox;  //Declare un une boite 
GError    *error = NULL;

int main(int argc,char *argv[])
{
      GtkWidget *buttonQuit; //Declarre un bouton
      GtkWidget *buttonGen; //Declarre un bouton
      
      if( ! g_thread_supported() )
	g_thread_init( NULL );
      gdk_threads_init();
      gdk_threads_enter();

      gtk_init (&argc, &argv); //Initialise GTK
      
      window = gtk_window_new (GTK_WINDOW_TOPLEVEL); //Creer uen fenetre GTK 
      gtk_widget_set_name (window, "Window");

      vbox = gtk_vbox_new (FALSE, 0); //Crer une nouvelle boite
      gtk_container_add (GTK_CONTAINER (window), vbox); //Rajoute la boite a la fenetre
      gtk_widget_show (vbox);  //Affiche la boite

      /* Create the drawing area */

      drawing_area = gtk_drawing_area_new (); //Crer une zone de dessins
      gtk_drawing_area_size (GTK_DRAWING_AREA (drawing_area), 200, 200); //Initialise la zone de dessins
      gtk_box_pack_start (GTK_BOX (vbox), drawing_area, TRUE, TRUE, 0); //Ajoute la zone de dessin a la boute

     
  
      gtk_widget_set_usize(drawing_area,WIDTH,HEIGHT);

      gtk_signal_connect(GTK_OBJECT(drawing_area),"event",
	      GTK_SIGNAL_FUNC(eventDraw),NULL);
      buttonQuit = gtk_button_new_with_label ("Quitter"); //Creer un bouton avec le label Quitter
      buttonGen = gtk_button_new_with_label ("Generer"); //Creer un bouton avec le label Quitter
      gtk_box_pack_start (GTK_BOX (vbox), buttonQuit, FALSE, FALSE, 0); //Initialise le bouton et l'ajoute a la boite
      gtk_box_pack_start (GTK_BOX (vbox), buttonGen, FALSE, FALSE, 0); //Initialise le bouton et l'ajoute a la boite

      gtk_signal_connect (GTK_OBJECT (buttonQuit), "clicked",
                      (GtkSignalFunc) quit, NULL);
      gtk_signal_connect (GTK_OBJECT (buttonGen), "clicked",
                      (GtkSignalFunc) generate, NULL);
      gtk_widget_show (drawing_area); //Affiche la zone de dessin
      gtk_widget_show (buttonGen); //Affiche le bouton
      gtk_widget_show (buttonQuit); //Affiche le bouton
      gtk_widget_show (window); //Affiche la fenetre
      gtk_main();
      gdk_threads_leave();
      return 0;
}

gboolean eventDraw(GtkWidget *widget,
        GdkEvent *event,gpointer data) 
{
//     drawArcs(widget,0,FALSE);
//     drawArcs(widget,80,TRUE);
//     drawCircle(0,0,FALSE);
//     drawCircle(0,80,TRUE);
    return(TRUE);
}
void quit (){
   gtk_exit (0); //Quitte l'aplication
}

static gpointer thread_func (gpointer data)
{
      gdk_threads_enter();
      generate_topologie (10,70,30);
      gdk_threads_leave();
      return NULL;
}


void generate (){
//    GList *children, *iter;
//    children = gtk_drawing_get_children(drawing_area);
//    for(iter = children; iter != NULL; iter = g_list_next(iter))
//      gtk_widget_destroy(GTK_WIDGET(iter->data));
//    g_list_free(children);
    gtk_widget_queue_draw (drawing_area);
    g_thread_create( thread_func, NULL, FALSE, &error );
//    drawCircle(0,0,FALSE);
//    drawCircle(0,80,TRUE);
}

gboolean callback_generate (void *t){
  struct data_thread *dt = (struct data_thread *) t;
  struct point * points = dt->data;
  int i = 0;
  for (i = 0; i<dt->length;i++){
//       GtkWidget *label_time = gtk_label_new(""+points[i].id);
//       gtk_fixed_put(drawing_area->window, label_time, 20, 20);
      gdk_draw_arc(drawing_area->window,
	drawing_area->style->black_gc,
	FALSE,
	(points[i].x * (WIDTH_DRAW - 10) + 10) ,(points[i].y * (HEIGHT_DRAW- 10)+10),
	15,15,
	0,360 * 64);
  }
  return(FALSE);
}


static void drawArcs(GtkWidget *widget,gint y,gint fill)
{
    gdk_draw_arc(widget->window,
            widget->style->black_gc,
            fill,
            0,y,
            60,60,
            0,360 * 64);
    gdk_draw_arc(widget->window,
            widget->style->black_gc,
            fill,
            80,y,
            60,60,
            135 * 64,90 * 64);
    gdk_draw_arc(widget->window,
            widget->style->black_gc,
            fill,
            110,y,
            60,60,
            135 * 64,-270 * 64);

    gdk_draw_arc(widget->window,
            widget->style->black_gc,
            fill,
            190,y,
            100,60,
            0,360 * 64);
    gdk_draw_arc(widget->window,
            widget->style->black_gc,
            fill,
            310,y,
            100,60,
            135 * 64,90 * 64);
    gdk_draw_arc(widget->window,
            widget->style->black_gc,
            fill,
            350,y,
            100,60,
            135 * 64,-270 * 64);
}

void drawCircle(gint x, gint y,gint fill)
{
    gdk_draw_arc(drawing_area->window,
            drawing_area->style->black_gc,
            fill,
            0,y,
            60,60,
            0,360 * 64);
    gdk_draw_arc(drawing_area->window,
            drawing_area->style->black_gc,
            fill,
            80,y,
            60,60,
            135 * 64,90 * 64);
    gdk_draw_arc(drawing_area->window,
            drawing_area->style->black_gc,
            fill,
            110,y,
            60,60,
            135 * 64,-270 * 64);

    gdk_draw_arc(drawing_area->window,
            drawing_area->style->black_gc,
            fill,
            190,y,
            100,60,
            0,360 * 64);
    gdk_draw_arc(drawing_area->window,
            drawing_area->style->black_gc,
            fill,
            310,y,
            100,60,
            135 * 64,90 * 64);
    gdk_draw_arc(drawing_area->window,
            drawing_area->style->black_gc,
            fill,
            350,y,
            100,60,
            135 * 64,-270 * 64);
}

gint eventDelete(GtkWidget *widget,
        GdkEvent *event,gpointer data) {
    return(FALSE);
}
gint eventDestroy(GtkWidget *widget,
        GdkEvent *event,gpointer data) {
    gtk_main_quit();
    return(0);
}