#include "main_window.h"

#define WIDTH 300
#define HEIGHT 300
#define WIDTH_DRAW 250
#define HEIGHT_DRAW 250
#define ARC_LENGTH 7


GtkWidget *drawing_area = NULL ;
gint height_vbox;
gint width_vbox;
GtkWidget *fenetre_principale = NULL; 
GtkRequisition req;
int thread_sched_created = FALSE;
GThread* thread_sched;
const gchar *icon = "./media/coconut-icon.png\0";
const char* name = "/smarthomegraph.data";

GAsyncQueue *   simul_queue;

// coconode window builder:
GtkBuilder *builder = NULL; 
GError *error = NULL; 

// gui glade source file:
gchar *filename = NULL; 

// multiples generator files:
GSList* files = NULL;

// GTK widget list to show all generator files
// GtkListStore *generator_files_list;

GtkTextView* generator_files_view;

gboolean new_files = FALSE;
gboolean updating_legend = FALSE;
type_to_color *typ_to_col=NULL;
int length_type=0;

int dad2child[2];
int child2dad[2];

talk2Child* tc;
talk2Dad* td;


/* Return value of timeout fonction linked to progress bar */
int timeout_fct; 

/* The file selection widget and the string to store the chosen filename */

// void store_filename (GtkWidget *widget, gpointer user_data) {
//    GtkWidget *file_selector = GTK_WIDGET (user_data);
//    const gchar *selected_filename;
// 
//    selected_filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (file_selector));
//    printf ("Selected filename: %s\n", selected_filename);
// }

void create_file_selection (void) {
   GtkWidget *pFileSelection;
//    gchar *sChemin;

// 
//    /* Create the selector */
//    
//    file_selector = gtk_file_selection_new ("Please select a file for editing.");
//    gtk_file_selection_set_select_multiple(file_selector,TRUE);
//    g_signal_connect (GTK_FILE_SELECTION (file_selector)->ok_button,
//                      "clicked",
//                      G_CALLBACK (store_filename),
//                      file_selector);
//    			   
//    /* Ensure that the dialog box is destroyed when the user clicks a button. */
//    
//    g_signal_connect_swapped (GTK_FILE_SELECTION (file_selector)->ok_button,
//                              "clicked",
//                              G_CALLBACK (gtk_widget_destroy), 
//                              file_selector);
// 
//    g_signal_connect_swapped (GTK_FILE_SELECTION (file_selector)->cancel_button,
//                              "clicked",
//                              G_CALLBACK (gtk_widget_destroy),
//                              file_selector); 
//    
//    /* Display that dialog */
//    
//    gtk_widget_show (file_selector);
   

   
   /* Creation de la fenetre de selection */
    pFileSelection = gtk_file_chooser_dialog_new("Ouvrir...",
						 NULL ,
						 GTK_FILE_CHOOSER_ACTION_OPEN,GTK_STOCK_CANCEL, 
						 GTK_RESPONSE_CANCEL,GTK_STOCK_OPEN, 
						 GTK_RESPONSE_OK,
						 NULL);
    gtk_file_chooser_set_select_multiple (GTK_FILE_CHOOSER(pFileSelection),TRUE);
    /* Affichage fenetre */
    switch(gtk_dialog_run(GTK_DIALOG(pFileSelection)))
    {
        case GTK_RESPONSE_OK:
            /* Recuperation du chemin */
	    files = gtk_file_chooser_get_files(GTK_FILE_CHOOSER(pFileSelection));
      update_generator_files_list(generator_files_view, files);
	    new_files = TRUE;
	    updating_legend = TRUE;

//             sChemin = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(pFileSelection));
//             pDialog = gtk_message_dialog_new(GTK_WINDOW(pFileSelection),
//                 GTK_DIALOG_MODAL,
//                 GTK_MESSAGE_INFO,
//                 GTK_BUTTONS_OK,
//                 "Chemin du fichier :\n%s", sChemin);
//             gtk_dialog_run(GTK_DIALOG(pDialog));
//             gtk_widget_destroy(pDialog);
// 	    g_free(sChemin);
            break;
        default:
            break;
    }
    gtk_widget_destroy(pFileSelection);
}

gboolean on_child_said(GIOChannel *source,GIOCondition condition,gpointer data){
  char buf[100]; 
  int choice;   
  
  read(g_io_channel_unix_get_fd((GIOChannel * ) data),buf,sizeof(buf));
  choice = atoi(buf);

  switch(choice){
    case(STOP):{
      // printf("Dad heard : %s\n",buf);
      free(tc);
      free(td);

    }break;
    default:{
      // printf("Dad heard : %s\n",buf);
    }break;
  }
  
  return TRUE;
}

int main(int argc, char *argv []) 
{ 
  int width, height;

/*
	if( ! g_thread_supported() )
	  g_thread_init( NULL );*/
	gdk_threads_init();
	gdk_threads_enter();
	
	/* Initialisation de la librairie Gtk. */ 
	gtk_disable_setlocale();
	gtk_init(&argc, &argv); 

	/* Ouverture du fichier Glade de la fenêtre principale */ 
	builder = gtk_builder_new(); 
	
	/* Création du chemin complet pour accéder au fichier gui.glade. */ 
	/* g_build_filename(); construit le chemin complet en fonction du système */ 
	/* d'exploitation. ( / pour Linux et \ pour Windows) */ 
	filename =  g_build_filename ("./src/gui/gui.glade", NULL); 

		/* Chargement du fichier gui.glade. */ 
	gtk_builder_add_from_file (builder, filename, &error); 
	g_free (filename); 
	if (error) 
	{ 
		gint code = error->code; 
		printf("ERROR : %s\n", error->message); 
		g_error_free (error); 
		return code; 
	} 

	/* Récupération du pointeur de la fenêtre principale */ 
	fenetre_principale = GTK_WIDGET(gtk_builder_get_object (
						builder, "MainWindow")); 
	gtk_window_get_size(GTK_WINDOW(fenetre_principale), &width, &height);
	      gtk_window_set_icon_from_file(GTK_WINDOW(fenetre_principale),
						      icon, NULL);

	gtk_widget_set_size_request(GTK_WIDGET(gtk_builder_get_object (builder, "legend")), 50, 70);
	/* Get pointer of GtkListStore* generator_files_list */
	// generator_files_list = GTK_LIST_STORE(gtk_builder_get_object(
	//           builder, "generator_files_list"));
	generator_files_view = GTK_TEXT_VIEW(gtk_builder_get_object(
		  builder, "generator_files_view"));
	gtk_widget_set_size_request(GTK_WIDGET(generator_files_view), 50, 60);
	// gtk_list_store_set_column_types(generator_files_list, 1, G_TYPE_STRING);
	  // generator_files_list = gtk_list_store_new(1, G_TYPE_STRING);

	update_generator_files_list(generator_files_view, files);



	/* Affectation du signal "destroy" à la fonction gtk_main_quit(); pour la */ 
	/* fermeture de la fenêtre. */ 
	g_signal_connect (G_OBJECT (fenetre_principale), "destroy", 
		(GCallback)gtk_main_quit, NULL); 

	/*connect callbacks to items of menubar*/

      g_signal_connect (G_OBJECT (GTK_WIDGET(gtk_builder_get_object (builder, "new_simulation"))), 
	"activate", 
	(GCallback)cb_menu_new_simulation, builder);
  
	// Export configuration:
	g_signal_connect (G_OBJECT (GTK_WIDGET(gtk_builder_get_object (builder, "export_config"))), 
		"activate", 
		(GCallback)cb_menu_export_config, builder); 

	// Import configuration:
	g_signal_connect (G_OBJECT (GTK_WIDGET(gtk_builder_get_object (builder, "import_config"))), 
		"activate", 
		(GCallback)cb_menu_import_config, builder); 

	drawing_area = gtk_drawing_area_new (); /* Creer une zone de dessin */
 	// gtk_drawing_area_size (GTK_DRAWING_AREA (drawing_area),WIDTH,HEIGHT); /* Initialiser la zone de dessin */
	gtk_drawing_area_size (GTK_DRAWING_AREA (drawing_area),
      (int)(width * 0.3),
      (int)(height * 0.5));

  gtk_box_pack_start (GTK_BOX (GTK_WIDGET(gtk_builder_get_object (
		    builder, "topo_image"))), drawing_area, TRUE, TRUE, 0); /* Ajouter la zone de dessin a la boite */

    gtk_progress_set_value(GTK_PROGRESS(GTK_WIDGET(gtk_builder_get_object (
        builder, "monitor_progressbar"))), 0.0); 


	gtk_signal_connect (
	  GTK_OBJECT (GTK_WIDGET(gtk_builder_get_object (builder, "generate"))), 
	  "clicked",(GtkSignalFunc) generate, NULL);
	gtk_signal_connect (
	  GTK_OBJECT (GTK_WIDGET(gtk_builder_get_object (builder, "start"))), 
	  "clicked",(GtkSignalFunc) start, NULL);
      gtk_signal_connect (
	GTK_OBJECT (GTK_WIDGET(gtk_builder_get_object (builder, "pause"))), 
	"clicked",(GtkSignalFunc) pause_simul, NULL);
      gtk_signal_connect (
	GTK_OBJECT (GTK_WIDGET(gtk_builder_get_object (builder, "stop"))), 
	"clicked",(GtkSignalFunc) stop_simul, NULL);
      gtk_signal_connect (
	GTK_OBJECT (GTK_WIDGET(gtk_builder_get_object (builder, "restart"))), 
	"clicked",(GtkSignalFunc) restart_simul, NULL);
	gtk_signal_connect (
	  GTK_OBJECT (GTK_WIDGET(gtk_builder_get_object (builder, "generator"))), 
	  "clicked",(GtkSignalFunc) create_file_selection, NULL);

  /* simul_queue to store asynchronously simulations */
  simul_queue = g_async_queue_new();
  init_jvm_for_simulation();

	/* Affichage de la fenêtre principale. */ 
	gtk_widget_show_all (fenetre_principale); 	
	gtk_main(); 
	gdk_threads_leave();
	return EXIT_SUCCESS; 
} 

void generate(){
//     printf("nom %s \n",gtk_file_chooser_get_uri(GTK_FILE_CHOOSER(GTK_WIDGET(
// 			    gtk_builder_get_object (builder, "generator")))));
    gdouble nb_nodes = gtk_spin_button_get_value(GTK_SPIN_BUTTON(GTK_WIDGET(
			      gtk_builder_get_object (builder, "nbNodes"))));
    gdouble percentage_sensors = gtk_spin_button_get_value(GTK_SPIN_BUTTON(GTK_WIDGET(
			  gtk_builder_get_object (builder, "sensorpt"))));
    gdouble percentage_actuators = gtk_spin_button_get_value(GTK_SPIN_BUTTON(GTK_WIDGET(
		      gtk_builder_get_object (builder, "actuatorpt"))));
    // printf("nbNodes : %f\n",nb_nodes);
    // printf("sensorpt : %f\n",percentage_sensors);
    // printf("actuatorpt : %f\n",percentage_actuators);
    gtk_widget_queue_draw (drawing_area); //effacer tous les elements de la zone de dessin
    Generation_infos *data = malloc(sizeof(Generation_infos));
    data->nb_nodes=nb_nodes;
    data->percentage_sensors=percentage_sensors;
    data->percentage_actuators=percentage_actuators;
    data->new_files = new_files;
    new_files = FALSE;
    data->files = files;
    data->repository =  gtk_file_chooser_get_filename(
                (GtkFileChooser*) gtk_builder_get_object(builder, 
                    "results_d"));
    g_thread_new("generate",thread_generate_topologie,data);
}

int existing_type (char *type,GdkColor *color){
    int i=0;
    while(i<length_type){
      if(strcmp(type,typ_to_col[i].type)==0){
	break;
      }
      i++;
    }
    if(i == length_type){
      if(length_type==0){
	 typ_to_col = malloc ((length_type+1)*sizeof(type_to_color));
      }
      else{
	  typ_to_col = realloc (typ_to_col, (length_type+1)*sizeof(type_to_color));
      }
      color->red = rand()*65535;
      color->green = rand()*65535;
      color->blue = rand()*65535;
      // printf("%d %d %d \n",color->red, color->green,color->blue);
      typ_to_col[length_type].type = malloc ((strlen(type)+1)*sizeof(char));
      memcpy(typ_to_col[length_type].type,type,(strlen(type)+1)*sizeof(char));
      typ_to_col[length_type].color = gdk_color_to_string(color);
      length_type++;
      return 0;
    }
    else{
      if(gdk_color_parse (typ_to_col[i].color, color)){
	return 1;
      }
      else{
	/*ERREUR*/
	return -1;
      }
    }
    return -1;
}

gboolean maj_loadBar (void *t)
{
    float *value = (float *)t;
    gtk_progress_set_value(GTK_PROGRESS(GTK_WIDGET(gtk_builder_get_object (
    builder, "monitor_progressbar"))),  (*value)*100); 
    free(value);
    return FALSE;
}

gboolean maj_eclapsed_time (void *t)
{
  char buffer [50];
  long value = (long) t;
  sprintf (buffer, "%lu", value);
  gtk_label_set_label(GTK_LABEL(GTK_WIDGET(gtk_builder_get_object (
  builder, "eclapsed_time"))),  buffer); 
  return FALSE;
}

gboolean callback_generate (void *t)
{
  struct data_thread *dt = (struct data_thread *) t;
  if(dt == NULL){
    printf("dt == NULL\n");
  }
  struct point * points = (struct point *) dt->data;

  int i = 0;
  PangoLayout *layout;
  PangoContext *context;
  GdkColor color;
  cairo_t *cr_drawing_area;
  GtkTextBuffer *buffer;
  GtkTextMark *cursor;
  GtkTextIter iter,start,end;
  GtkTextChildAnchor *anchor;
  GtkWidget *drawing_legend;
  int existing_color;
  gint w = gdk_window_get_width (drawing_area->window);
  gint h = gdk_window_get_height(drawing_area->window);
  context = gdk_pango_context_get ();
  layout = pango_layout_new (context);

  if(updating_legend == TRUE || 
    gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(
      gtk_builder_get_object (builder, "choice_changing")
    ))
  )
  {
      if(length_type != 0)
      {
        for(i = 0; i < length_type; i++){
          free(typ_to_col[i].type);
          free(typ_to_col[i].color);
        }
    	  free(typ_to_col);
    	  length_type = 0;
      }	
      buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (
	GTK_WIDGET(gtk_builder_get_object (builder, "legend"))));
//       cursor = gtk_text_buffer_get_insert (buffer);
//       gtk_text_buffer_get_start_iter (buffer,&iter);
//       g_list_free(gtk_text_child_anchor_get_widgets(gtk_text_iter_get_child_anchor(&iter)));
      gtk_text_buffer_get_start_iter (buffer,&start); 
      gtk_text_buffer_get_end_iter (buffer,&end); 
      gtk_text_buffer_delete (buffer,&start,&end); 
      gtk_text_buffer_get_start_iter(buffer, &iter);
      updating_legend = FALSE;
  }
  cr_drawing_area = gdk_cairo_create(gtk_widget_get_window(drawing_area));
  

  for (i = 0; i<dt->length;i++)
  {
//       printf("%f %f\n",(points[i].x * (w - (w*10/100)) + 10),
// 	     (points[i].y * (h- (h*10/100))+10));        
      
//       pango_layout_set_text (layout, points[i].type, strlen (points[i].type));
//       gdk_draw_layout (drawing_area->window, drawing_area->style->black_gc, (points[i].x * (w - (w*10/100)) + 10) ,
// 		   (points[i].y * (h- (h*10/100))+10), layout);
      points[i].x = (points[i].x*(w-(2*(ARC_LENGTH+1)))) + (ARC_LENGTH+1);
      points[i].y = (points[i].y*(h-(2*(ARC_LENGTH+1)))) + (ARC_LENGTH+1);  
      cairo_set_line_width(cr_drawing_area, 1);  
      gdk_color_parse ("black", &color);
      gdk_cairo_set_source_color(cr_drawing_area,&color); 
      cairo_arc(cr_drawing_area, points[i].x,points[i].y, ARC_LENGTH, 0, 2 * M_PI);
      cairo_stroke_preserve(cr_drawing_area);
      existing_color = existing_type(points[i].type,&color);
      gdk_cairo_set_source_color(cr_drawing_area,&color); 
      cairo_fill(cr_drawing_area); 

      if(existing_color == 0)
      {
	  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (
	    GTK_WIDGET(gtk_builder_get_object (builder, "legend")))
	  );
	  cursor = gtk_text_buffer_get_insert (buffer);
	  gtk_text_buffer_get_iter_at_mark (buffer, &iter, cursor);
	  anchor = gtk_text_buffer_create_child_anchor (buffer, &iter);
	  drawing_legend = gtk_drawing_area_new ();      
	  gtk_drawing_area_size (GTK_DRAWING_AREA (drawing_legend),
				 ARC_LENGTH,ARC_LENGTH);
	  gtk_widget_show (drawing_legend);
	  gtk_text_view_add_child_at_anchor (GTK_TEXT_VIEW(
	    GTK_WIDGET(gtk_builder_get_object (builder, "legend"))),
		drawing_legend, anchor
	  );
/*	  cr_drawing_legend = gdk_cairo_create(
	    gtk_widget_get_window(drawing_legend));
	  cairo_set_line_width(cr_drawing_legend, 1);  
//       gdk_color_parse ("black", &color);
	  gdk_cairo_set_source_color(cr_drawing_legend,&color); 
	  cairo_arc(cr_drawing_legend, 0,0, ARC_LENGTH, 0, 2 * M_PI);
	  cairo_stroke_preserve(cr_drawing_legend);
	  gdk_cairo_set_source_color(cr_drawing_legend,&color); 
	  cairo_fill(cr_drawing_legend);   */ 
	  gtk_widget_modify_bg(drawing_legend,drawing_legend->state,&color);
	  gtk_text_buffer_insert (buffer, &iter, points[i].type, -1);
	  gtk_text_buffer_insert (buffer, &iter, "\n", 1);
// 	  cairo_destroy (cr_drawing_legend);
      }
//       gdk_cairo_set_source_color
//       gtk_widget_modify_fg(drawing_area,drawing_area->state,&color);
//       printf("%d %d %d\n",color.red, color.green, color.blue);  
//       gdk_draw_arc(drawing_area->window,
// 	drawing_area->style->fg_gc[drawing_area->state],
// 	TRUE,
// 	(points[i].x * (w - (w*10/100)) + 10) ,
// 		   (points[i].y * (h- (h*10/100))+10),
// 	15,15,
// 	0,360 * 64);
//       gdk_draw_arc(drawing_area->window,
// 	drawing_area->style->black_gc,
// 	FALSE,
// 	(points[i].x * (w - (w*10/100)) + 10) ,
// 		   (points[i].y * (h- (h*10/100))+10),
// 	15,15,
// 	0,360 * 64);
  }
  cairo_destroy (cr_drawing_area);      
  g_object_unref (layout);
  g_object_unref (context);

  for(i=0;i<dt->length;i++){
    free((points+i)->type);
  }
  free(points);

  free(dt);
  return(FALSE);
}


// void start (){
// 
//   pipe(dad2child);
//   pipe(child2dad);
// 
//   GIOChannel * fromChild = g_io_channel_unix_new(child2dad[0]);
//      
//     Start_infos *data = malloc(sizeof(Start_infos));
//     data->toDad = g_io_channel_unix_new(child2dad[1]);
//     data->fromDad = g_io_channel_unix_new(dad2child[0]);
Start_infos* prepare_simul_data(){
    Start_infos *data = malloc(sizeof(Start_infos));
    // data->toDad = g_io_channel_unix_new(child2dad[1]);
    // data->fromDad = g_io_channel_unix_new(dad2child[0]);

    int hours = gtk_spin_button_get_value_as_int(
                (GtkSpinButton*) gtk_builder_get_object(builder, 
                    "simul_hours"));
    
    int minutes = gtk_spin_button_get_value_as_int(
                (GtkSpinButton*) gtk_builder_get_object(builder, 
                    "simul_minutes"));
    
    minutes = minutes + (hours*60);
    
    data->minutes = minutes;
    
    data->repeat = gtk_spin_button_get_value_as_int(
                (GtkSpinButton*) gtk_builder_get_object(builder, 
                    "repeat_times"));
    data->protocol = gtk_file_chooser_get_filename(
                (GtkFileChooser*) gtk_builder_get_object(builder, 
                    "protocol"));
    int schedule_hours = gtk_spin_button_get_value_as_int(
                (GtkSpinButton*) gtk_builder_get_object(builder, 
                    "schedule_hours"));
    int schedule_minutes = gtk_spin_button_get_value_as_int(
                (GtkSpinButton*) gtk_builder_get_object(builder, 
                    "schedule_minutes"));
    schedule_minutes = schedule_minutes + (schedule_hours*60);
    
    data->scheduled_date = time(NULL); // seconds since 1th January 1970
    // data->scheduled_date += schedule_minutes*60; // scheduled time to begin simulation
    #ifdef BEGIN_BURST
    data->scheduled_date += schedule_minutes;
    #else
    data->scheduled_date += schedule_minutes*60; // scheduled time to begin simulation (in seconds)
    #endif
    
    data->repository =  gtk_file_chooser_get_filename(
                (GtkFileChooser*) gtk_builder_get_object(builder, 
                    "results_d"));
    data->nodes = gtk_spin_button_get_value_as_int
                    ((GtkSpinButton*) gtk_builder_get_object(builder, 
                        "nbNodes"));
//     init();
//     g_thread_new("start",thread_simulation,data);
// //     g_thread_create( thread_simulation, data, FALSE, &error );
// 
//     // wait for child to go to bed
// //     g_io_add_watch(fromChild, G_IO_IN | G_IO_HUP | G_IO_ERR,
// //           (GIOFunc)on_child_said,fromChild);
// 
// //     write(dad2child[1],"1",sizeof(int));
//     start_simulation_message();
    return data;
}

void start (){
  Start_infos* simul_data;
  int i, repeat;

  if(!thread_sched_created){ //thread_sched not created, create a thread
    /* to communicate with thread_sched via pipe*/
    pipe(dad2child);
    pipe(child2dad);

    tc = malloc(sizeof(talk2Child));
    td = malloc(sizeof(talk2Dad));
    tc->toChild = g_io_channel_unix_new(TO_CHILD);
    tc->fromChild = g_io_channel_unix_new(FROM_CHILD);

    td->toDad = g_io_channel_unix_new(TO_DAD);
    td->fromDad = g_io_channel_unix_new(FROM_DAD);

    g_io_add_watch(tc->fromChild, G_IO_IN | G_IO_HUP | G_IO_ERR,
            (GIOFunc)on_child_said,tc->fromChild);

    thread_sched = g_thread_new("thread_scheduler",thread_scheduler,td);
    thread_sched_created = TRUE;

  }

  repeat = gtk_spin_button_get_value_as_int(
                (GtkSpinButton*) gtk_builder_get_object(builder, 
                    "repeat_times"));
  for(i = 0; i < repeat; i++){
    simul_data = prepare_simul_data();
    simul_data->rang = i+1;
    // add data into simul_queue
    g_async_queue_push_sorted (simul_queue, simul_data, (GCompareDataFunc) comparator_simul_data, NULL);  
  }

    // notify thread_sched that a new simul has been add
    write(TO_CHILD,"1",sizeof(int));

    // g_print("START\n");
}


void pause_simul(){
  // g_print("PAUSE\n");
  write(TO_CHILD,"2",sizeof(int));
}

void stop_simul(){
  // g_print("STOP\n");
  write(TO_CHILD,"3",sizeof(int));
}

void restart_simul(){
  // g_print("RESTART\n");
  write(TO_CHILD,"4",sizeof(int));
} 
/*
void stop_simul(){
    stop_simulation_message();
//     write(dad2child[1],"4",sizeof(int));
}*/
