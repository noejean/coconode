#ifndef __TOPOLOGIE_H__
#define __TOPOLOGIE_H__

#include <stdio.h>
#include <gtk/gtk.h>
#include "cercle.h"
#include "gen.h"
#include <float.h>

void generate_topologie (int number_of_sensors, int percentage_sensors, 
		int percentage_actuators);
#endif