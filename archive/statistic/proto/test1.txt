# NumberOfGraphs=2
# Titles="This is the title1" "This is the title2"
# Types=line	histo
# Xlabels="Time" "Time"
# Ylabels="NB_packets" "NB_lost"
# DataOrder=sequential parallel

# Data
Graph1-X	0
Graph1-X	1
Graph1-X	2
Graph1-X	3
Graph1-X	4
Graph1-X	5
Graph1-X	6
Graph1-X	7
Graph1-X	8
Graph1-X	9
Graph1-X 	10
Graph1-X 	11
Graph1-X 	12

Graph1-Y	0
Graph1-Y	1
Graph1-Y	2
Graph1-Y	3
Graph1-Y	4
Graph1-Y	5
Graph1-Y	6
Graph1-Y	7
Graph1-Y	8
Graph1-Y	9
Graph1-Y 	10
Graph1-Y 	11
Graph1-Y 	12

Graph2 	0	12
Graph2 	1	11
Graph2 	2	10
Graph2 	3	9
Graph2 	4	8
Graph2 	5	7
Graph2 	6	6
Graph2 	7	5
Graph2 	8	4
Graph2 	9	3
Graph2 	10 	2
Graph2 	11 	1
Graph2 	12 	0

