#include "simulation_control.h"
#define COOJA_TESTS "/"

#define TIMEOUT 1 // in seconds

extern const char* name;
void *lib_handle;
GMainLoop *sim_loop;

int is_cooja_running;
int is_paused;
int is_stopped; 
int copy_doned;
long current_simulation_duration_ms = 0;
float *time_percentage;

char input_data_file[256] = ""; 
char new_file[256] = ""; 
char new_repository_tmp[256] = ""; 
char new_repo[256] = ""; 
char new_repo_results[256] = ""; 
char contiki_file[256] = ""; 
char output_file[256] = ""; 
char  protocol[256] = ""; 

/* List of pending simulations */
extern GAsyncQueue* simul_queue;

extern int thread_sched_created;

talk2Dad td;

int (*ptrfunc)(char *input_file, char *output_file, int simulation_time, char *contiki_repo);

int timer = -1;


int cp(const char *from_name, const char *to_name)
{
  FILE *from, *to;
  char ch;

  /* open source file */
  if((from = fopen(from_name, "rb"))==NULL) {
    printf("Cannot open source file.\n");
    return -1;
  }

  /* open destination file */
  if((to = fopen(to_name, "wb"))==NULL) {
    printf("Cannot open destination file.\n");
    return -1;
  }

  /* copy the file */
  while(!feof(from)) {
    ch = fgetc(from);
    if(ferror(from)) {
      printf("Error reading source file.\n");
      return -1;
    }
    if(!feof(from)) fputc(ch, to);
    if(ferror(to)) {
      printf("Error writing destination file.\n");
      return -1;
    }
  }

  if(fclose(from)==EOF) {
    printf("Error closing source file.\n");
    return -1;
  }

  if(fclose(to)==EOF) {
    printf("Error closing destination file.\n");
    return -1;
  }
  return 0;
}


int length(int n)
{
  int i = 0;
  if(n<10)
    return 1;
    
  while(n>=1)
  {
    n = n / 10;
    i++;
  }
  return i;
}

char* last_element(char *repo,char* delimit){
  char * pch;
  char * tmp=NULL;
  pch = strtok (repo,delimit);
  while (pch != NULL)
  {
    tmp= realloc(tmp,sizeof(char)*(strlen(pch)+1));
    strcpy(tmp,"");
    strcpy(tmp,pch);
//     memcpy(tmp,pch,(strlen(pch)+1)*sizeof(char));
    pch = strtok (NULL, delimit);
  }
  return tmp;
}


void start_simulation(gpointer data){
  
  Start_infos* g_data = (Start_infos*) data;

 
  
  void *lib_handle = NULL;
  int (*ptrfunc_coojaTopGen) (char *input_file, 
                                  char *output_file, 
                                  int simulation_time, 
                                  char *contiki_repo);
  char tmp[256] ="";
  // printf("Child : Start simulation()\n");
    strcpy(input_data_file,"");
    memcpy(input_data_file,g_data->repository,strlen(g_data->repository)*sizeof(char));
    memcpy((input_data_file+strlen(g_data->repository)),name,(strlen(name)+1)*sizeof(char));
     strcpy(new_file,"");
    snprintf(new_file,20+length(g_data->nodes)+length(g_data->minutes), 
            "%i_nodes_%i_minutes.csc",g_data->nodes,g_data->minutes);
    snprintf(new_repository_tmp,100+length(g_data->nodes)+length(g_data->minutes), 
            "%i_nodes_%i_minutes",g_data->nodes,g_data->minutes);
    
    // printf("%d %d %s %s \n",length(g_data->nodes),length(g_data->minutes),new_file,new_repository_tmp);

    // new_repo = malloc((strlen(g_data->repository)+strlen(new_repository_tmp)+1)*sizeof(char));
    if( new_repo == NULL) g_print("ERROR : cannot malloc new_repo \n");
    strcpy(new_repo,"");
    memcpy(new_repo,g_data->repository,(strlen(g_data->repository)+1)*sizeof(char));
    strcat(new_repo,"/");
    memcpy((new_repo+strlen(g_data->repository)+1),new_repository_tmp,(strlen(new_repository_tmp)+1)*sizeof(char));
    
    if (mkdir (new_repo,S_IRWXU|S_IRGRP|S_IXGRP)!= 0) {
	perror("mkdir() error");
	printf("mkdir: impossible de créer le répertoire «%s»\n",new_repo);  
    }

    lib_handle = dlopen ("/tmp/user.so", RTLD_LAZY) ;
    if (lib_handle){
     printf("Dynamic library opening: Successful\n");  
    } else {
     printf ("[%s] Unable to open library: %s \n", "user.so", dlerror ()) ;
     exit (-1) ;
    }
    // output_file = malloc((strlen(new_repo)+strlen(new_file)+1)*sizeof(char));

    if( output_file == NULL) g_print("ERROR : cannot malloc output_file \n");
    strcpy(output_file,"");
    strcpy(output_file,new_repo);
    strcat(output_file,"/");
    strcat(output_file,new_file);
    ptrfunc_coojaTopGen = (void *) dlsym (lib_handle, "generate_csc_file") ;
    int error = (*ptrfunc_coojaTopGen)(input_data_file,output_file,g_data->minutes,g_data->protocol);

    // contiki_file = malloc((strlen(g_data->protocol)+strlen(COOJA_TESTS)+strlen(new_file)+1)*sizeof(char));
    if( contiki_file == NULL) g_print("ERROR : cannot malloc contiki_file \n");
    strcpy(contiki_file,"");
    strcpy(contiki_file,g_data->protocol);
    strcat(contiki_file,COOJA_TESTS);
    strcat(contiki_file,new_file);
    // protocol = last_element(g_data->protocol,"/");
    strcpy(tmp,g_data->protocol);
    strcpy(protocol, last_element(tmp,"/"));

    // new_repo_results = malloc((strlen(new_repo)+strlen(protocol)+1)*sizeof(char));
    if( new_repo_results == NULL) g_print("ERROR : cannot malloc new_repo_results \n");
    strcpy(new_repo_results,"");
    strcpy(new_repo_results,new_repo);
    strcat(new_repo_results,"/");
    strcat(new_repo_results,protocol);
     if (mkdir (new_repo_results,S_IRWXU|S_IRGRP|S_IXGRP) != 0){
	   perror("mkdir() error");
           printf("mkdir: impossible de créer le répertoire «%s»\n",new_repo_results);  
     }
    snprintf(new_repo_results+strlen(new_repo_results),13+length(g_data->rang), 
            "/%i_simulation",g_data->rang);
     if (mkdir (new_repo_results,S_IRWXU|S_IRGRP|S_IXGRP) != 0){
	   perror("mkdir() error");
           printf("mkdir: impossible de créer le répertoire «%s»\n",new_repo_results);  
     }
    
    if(error >= 0)
    {
      if( cp(output_file,contiki_file) == 0)
      {
	  strcat(new_repo_results,"/results/"); 
	  if (mkdir (new_repo_results,S_IRWXU|S_IRGRP|S_IXGRP) != 0){
	    perror("mkdir() error");
            printf("mkdir: impossible de créer le répertoire «%s»\n",new_repo_results);  
	  }
	  else{
	    
	  }

      }else{
        printf("Erreur de copie");
      }
    }else{
      printf("Erreur de creation de fichier");
    }
    // ci = malloc(sizeof(Cooja_infos));
    // ci->contiki_file = malloc((strlen(contiki_file) +1)* sizeof(char));
    // memcpy(ci->contiki_file, contiki_file, (strlen(contiki_file) +1)*sizeof(char));
    // ci->protocol = malloc((strlen(g_data->protocol) +1)* sizeof(char));
    // memcpy(ci->protocol, g_data->protocol, (strlen(g_data->protocol) +1)*sizeof(char));
    // create a thread to run COOJA
    // g_thread_new("thread_cooja",thread_cooja, ci);
    // simulation(ci->contiki_file, ci->protocol);
    is_cooja_running = TRUE; 
    is_paused = FALSE;
    is_stopped = FALSE;
    copy_doned =  FALSE;
    simulation(contiki_file, g_data->protocol);
    current_simulation_duration_ms = g_data->minutes * 60 * 1000;
    // printf("End of start_simulation()\n");
}

void pause_simulation(gpointer data){
  is_paused = TRUE;
  pausing_simulation();
}

void stop_simulation(gpointer data){
  // stopping_simulation();
  // write(g_io_channel_unix_get_fd(g_data->toDad),"Yes",4);
  // g_print("Child respond : Yes\n");
  // g_main_loop_quit(sim_loop);
  if(is_cooja_running){
    is_cooja_running = FALSE; 
    is_paused = FALSE;
    is_stopped = TRUE;
    stopping_simulation();
  }

}


void restart_ctrl_simulation(gpointer data){
  is_cooja_running = TRUE; 
  is_paused = FALSE;
  is_stopped = FALSE;
  restart_simulation();
}

void init_jvm_for_simulation(){
init_jvm();

}

// comparator to sort simulations by schedule_minutes
int comparator_simul_data (Start_infos* sorted_data, Start_infos* new_data, gpointer user_data){
  int date1, date2;
  date1 = sorted_data->scheduled_date;
  date2 = new_data->scheduled_date;
  return (date1 > date2 ? +1 : date1 == date2 ? 0 : -1);
}

// look at the queue if a task is available on each minute
gboolean on_timer_expired(gpointer user_data){
  // try to pop a task from queue
  Start_infos *g_data;
  long time_sim =0;
  float resultat;
  time_percentage = malloc(sizeof(float));
  char cooja_log[256] = "";
  char cooja_testlog[256] ="";
  char topofile[256] = "";
  
  time_t now; // in seconds



  if(check_simulation()){
      time_sim = time_simulation ();
      // printf("time simulation = %l",time_sim);
      resultat = ((float)time_sim) / ((float)current_simulation_duration_ms);
      // printf("((float)time_sim) %f / ((float)current_simulation_duration_ms) %f",(float)time_sim , (float)current_simulation_duration_ms);
      *time_percentage = resultat;
      // printf("resultat = %f \n",*time_percentage);
      g_idle_add((GSourceFunc) maj_loadBar,(gpointer)time_percentage);
      g_idle_add((GSourceFunc) maj_eclapsed_time,(gpointer)time_sim);
  }

  if(!check_simulation()){
    if(!is_paused){
        stop_simulation(user_data);
        *time_percentage = 0.0;
        time_sim = 0.0;
        g_idle_add((GSourceFunc) maj_loadBar,(gpointer)time_percentage);
        g_idle_add((GSourceFunc) maj_eclapsed_time,(gpointer)time_sim);
    }
    is_cooja_running = FALSE;
  }
  if(is_stopped && !copy_doned){ // a simulation is finished
    // copy log file in to result directory
    strcat(cooja_log,new_repo_results);
    strcat(cooja_log,"COOJA.log");
    // printf("%s \n\n",cooja_log);
    cp("COOJA.log", cooja_log);
    strcat(cooja_testlog,new_repo_results);
    strcat(cooja_testlog,"COOJA.testlog");
     // printf("%s \n\n",cooja_testlog);
    cp("COOJA.testlog", cooja_testlog);

    strcat(topofile,new_repo);
    strcat(topofile,"/");
    strcat(topofile,"topology.data");
    cp(input_data_file, topofile);
    copy_doned = TRUE;
  }

  if((!is_cooja_running) && (!is_paused)){
    // if cooja is not running, try to pop a task  
    g_data = (Start_infos *) g_async_queue_try_pop (simul_queue);

    if(g_data != NULL){ // pop a task, either re-push into queue or run a simulation
      // cooja is free, now look if it's time to run a simulation
      now = time(NULL);
      if(now < g_data->scheduled_date){ // not yet, re-push into queue
        g_async_queue_push_sorted (simul_queue, g_data, (GCompareDataFunc) comparator_simul_data, NULL);
      }else{ // cooja is free and it's good time. Start a simulation ! 
        is_cooja_running = TRUE;
        // printf("is_cooja_running = TRUE \n");
        start_simulation(g_data);
      }
    }else{ // pop a task NULL, do nothing

    }
  }else{ // cooja is running, let it alone

  }    
  return TRUE; 
}

gboolean on_dad_said(GIOChannel *source,GIOCondition condition,gpointer data){
  char buf[100];  
  int choice;
  Start_infos *g_data; 
  time_t now;
  
  read(g_io_channel_unix_get_fd(source),buf,sizeof(buf));
  // g_print("Child heard : %s\n",buf);
  choice = atoi(buf);
  switch(choice){
    case (START):{
//       start_simulation(data);
//     sleep(15);
      write(g_io_channel_unix_get_fd(td.toDad),"1",2);
      // g_print("Child respond : OK START\n");

      if(is_paused){ // if cooja is paused, let it alone

      }else{
        if(!is_cooja_running){
          g_data = (Start_infos *) g_async_queue_try_pop (simul_queue);
          if(g_data == NULL){
            printf("Thread_scheduler : cannot pop from Asyncqueue\n");
          }else{
            // check if it's time to run simulation (defined by schedule_date)
            now = time(NULL);
            if(now < g_data->scheduled_date){ // not yet, re-push into queue
              g_async_queue_push_sorted (simul_queue, g_data, (GCompareDataFunc) comparator_simul_data, NULL);
              // arm a timer to come back at scheduled_date
              
            }else{ // cooja is free and it's a good time
              is_cooja_running = TRUE;
              // printf("is_cooja_running = TRUE \n");
              start_simulation(g_data);
            }
          }
        }else{ // if cooja is runnning, let it alone

        }
      }
        
    }break;
    case (PAUSE):{
      write(g_io_channel_unix_get_fd(td.toDad),"2",2);
      // g_print("Child respond : OK PAUSE\n");
      pause_simulation(data);
    }break;
    case (STOP):{
      write(g_io_channel_unix_get_fd(td.toDad),"3",2);
      // g_print("Child respond : OK STOP\n");
      stop_simulation(data);
    }break;
    case (RESTART):{
      write(g_io_channel_unix_get_fd(td.toDad),"4",2);
      // g_print("Child respond : OK RESTART\n");
      restart_ctrl_simulation(data);
    }break;
    default :break;
  }

//   // write back to dad:
//   write(g_io_channel_unix_get_fd(g_data->toDad),"Yes",4);
//   // g_print("Child respond : Yes\n");
//   return TRUE;
// }
// 
// gpointer thread_simulation (gpointer data){
//   
//   Start_infos* g_data = (Start_infos*) data;
//   // wait order from dad
// //   g_io_add_watch(g_data->fromDad,G_IO_IN | G_IO_HUP | G_IO_ERR,(GIOFunc)on_dad_said,data);
//   g_print("Child respond : Yes\n");
//  
//   while(1){
//       message* msg;
//       while(1){
// 	msg = g_async_queue_try_pop(queue);
// 	if (msg == NULL){
// 		break;
// 	}
// 	switch (msg->cmd) {
// 	  case START_S:
// 	      simulation("/home/florian/Bureau/Malisha-code/Malisha-code/automaticSimulations/5_nodes_15_minutes/autoGenTopology-5_nodes_15_minutes.csc"
//     ,"/home/florian/Bureau/Malisha-code/Malisha-code/ctk-load");
// 	      printf("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
// 		  break;
// 	  case PAUSE_S:
// 		  
// 		  break;
// 	  case STOP_S:
// 		  stopping_simulation();
// 		  break;
// 	  case RESTART_S:
// 		  
// 		  break;
// 	  }
// 	  free(msg);
// 	  if(!check_simulation ()){
// 	    printf("FIN");
// 	  }
//       }
//       
//   }
//   return NULL;
// }
// 
// message *new_message(Command cmd) {
// 	message *m = malloc(sizeof *m);
// 	if (m == NULL)
// 		g_error("out of memory");
// 	m->cmd = cmd;
// 	return m;
// }
// 
// void init() {
// 	queue = g_async_queue_new();
  return TRUE;
}

gpointer thread_scheduler (gpointer data){

  talk2Dad * tmp = (talk2Dad*) data;
  td.toDad = tmp->toDad;
  td.fromDad = tmp->fromDad;

  is_cooja_running = FALSE;
  is_paused = FALSE;
  is_stopped = FALSE;

  if(timer < 0){
    g_source_remove(timer);
    timer = g_timeout_add_seconds(TIMEOUT, (GSourceFunc) on_timer_expired,NULL);
  }

  if(td.fromDad == NULL){
    printf("thread_scheduler : fromDad == NULL \n");
  }else{
    g_io_add_watch(td.fromDad,G_IO_IN | G_IO_HUP | G_IO_ERR,(GIOFunc)on_dad_said,NULL);
  }
  
  sim_loop = g_main_loop_new(NULL,FALSE);
  g_main_loop_run(sim_loop);

  // while()

  thread_sched_created = FALSE;
  printf("thread_scheduler : end of thread_scheduler \n");
  return NULL;
}

// void send_message(message *m) {
// 	g_async_queue_push(queue, m);
// }
// 
// void start_simulation_message() {
// 	message *m = new_message(START_S);
// 	send_message(m);
// }
// void stop_simulation_message() {
// 	message *m = new_message(STOP_S);
// 	send_message(m);
// }
