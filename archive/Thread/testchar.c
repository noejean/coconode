#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <jni.h>
#define LONG 100

int main( int argc, char** argv ) {
    JavaVM*  jvm;      /* denotes a Java VM */
    JNIEnv*            env;      /* pointer to native method interface */
    JavaVMInitArgs     vm_args;
    JavaVMOption     options[10];
    jobject obj;
    int oi = 0;
    char log_config_path [100] ="/";
    options[oi++].optionString = "-Djava.compiler=/home/florian/Bureau/Malisha-code/java-6-sun"; 
    options[oi++].optionString = "-Djava.class.path=/home/flo/Bureau/Malisha-code/Malisha-code/ctk-load/tools/cooja/dist/cooja.jar:/home/flo/Bureau/Malisha-code/Malisha-code/ctk-load/tools/cooja/dist/lib/log4j.jar:/home/flo/Bureau/Malisha-code/Malisha-code/ctk-load/tools/cooja/dist/lib/jdom.jar";
//     :/System/Library/Frameworks/JavaVM.framework/Classes/classes.jar";
    vm_args.version = JNI_VERSION_1_6;
    vm_args.options = options;
    vm_args.nOptions = oi;
    vm_args.ignoreUnrecognized = 0;  
    long result = JNI_CreateJavaVM( &jvm, (void**)&env, &vm_args );
    if(result == JNI_ERR ) 
    {
      printf("Error invoking the JVM\n");
      //return 0;
    }
    
    if(getenv("JAVA_HOME") == NULL || strcmp(getenv("JAVA_HOME"),"")==0){
      printf("JAVA_HOME PAS BON %s\n",getenv("JAVA_HOME"));
    }
   jclass GUIclass = (*env)->FindClass( env, "se/sics/cooja/GUI");
   jclass class_File = (*env)->FindClass( env, "java/io/File");
   jclass class_URL = (*env)->FindClass(env, "java/net/URL");
   jclass class_Simulation = (*env)->FindClass(env, "se/sics/cooja/Simulation");
    if(GUIclass){
      printf("YES\n");
    }
    else{
      printf("NO\n");
    }
    
//         jmethodID midFILE = (*env)->GetMethodID(env, class_File, "<init>", "(Ljava/lang/String;)V");
//     jstring chaine = (*env)->NewStringUTF(env, "/home/flo/Bureau/Malisha-code/Malisha-code/automaticSimulations/5_nodes_30_minutes/autoGenTopology-5_nodes_30_minutes.csc");
//     jobject objet = (*env)->NewObject(env, class_File, midFILE, chaine);
    
    jfieldID fid_externalToolsUserSettingsFile = (*env)->GetStaticFieldID(env, GUIclass, "externalToolsUserSettingsFile", "Ljava/io/File;");
    
    jmethodID mid_FILE_Constr = (*env)->GetMethodID(env, class_File, "<init>", "(Ljava/io/File;Ljava/lang/String;)V");
    
    jmethodID mid_FILE_Constr_from_string = (*env)->GetMethodID(env, class_File, "<init>", "(Ljava/lang/String;)V");
    
    jstring user_home = (*env)->NewStringUTF(env, getenv("HOME"));
   
    jobject file = (*env)->NewObject(env, class_File, mid_FILE_Constr_from_string, user_home);
    
    jfieldID fid_EXTERNAL_TOOLS_USER_SETTINGS_FILENAME = (*env)->GetStaticFieldID(env, GUIclass, "EXTERNAL_TOOLS_USER_SETTINGS_FILENAME", "Ljava/lang/String;");
    
    jstring str_EXTERNAL_TOOLS_USER_SETTINGS_FILENAME = (*env)->GetStaticObjectField(env,GUIclass,fid_EXTERNAL_TOOLS_USER_SETTINGS_FILENAME);
    
    jobject setting_file = (*env)->NewObject(env, class_File, mid_FILE_Constr, file,str_EXTERNAL_TOOLS_USER_SETTINGS_FILENAME);
    if(setting_file){
      printf("YES \n");
    }
    else{
      printf("NO\n");
    }
//     externalToolsUserSettingsFile = new File(System.getProperty("user.home"), EXTERNAL_TOOLS_USER_SETTINGS_FILENAME);
    (*env)->SetStaticObjectField(env, GUIclass, fid_externalToolsUserSettingsFile, setting_file);
   jfieldID fidContikiPath = (*env)->GetStaticFieldID(env, GUIclass, "specifiedContikiPath", "Ljava/lang/String;");
   jstring contiki_path = (*env)->NewStringUTF(env, "/home/flo/Bureau/Malisha-code/Malisha-code/ctk-load");
   (*env)->SetStaticObjectField(env, GUIclass, fidContikiPath, contiki_path);
   if ((*env)->ExceptionOccurred(env)) {
	(*env)->ExceptionDescribe(env);
    }
 
    jfieldID fid_LOG_CONFIG_FILE = (*env)->GetStaticFieldID(env, GUIclass, "LOG_CONFIG_FILE", "Ljava/lang/String;");
    if(!fid_LOG_CONFIG_FILE){
      printf("YES fid log\n");
    }
    else{
      printf("NO fid log\n");
    }
    if ((*env)->ExceptionOccurred(env)) {
	(*env)->ExceptionDescribe(env);
    }
    jstring LOG_CONFIG_FILE = (*env)->GetStaticObjectField(env,GUIclass,fid_LOG_CONFIG_FILE);
    const char * log_config = (*env)->GetStringUTFChars(env,LOG_CONFIG_FILE,NULL);
    strcat(log_config_path,log_config);
    if(LOG_CONFIG_FILE){
      printf("%s\n",log_config_path);
    }
    else{
      printf("NO\n");
    }
    
    jmethodID getClass = (*env)->GetMethodID(env,GUIclass, "getClass", "()Ljava/lang/Class;" );
    jobject class_gui = (*env)->CallObjectMethod(env,GUIclass, getClass);
    jmethodID getResource = (*env)->GetMethodID( env,class_gui, "getResource", "(Ljava/lang/String;)Ljava/net/URL;" );
    jobject url = (*env)->CallObjectMethod(env,class_gui,getResource,(*env)->NewStringUTF(env,log_config_path));
    if(url){
      printf("YES URL");
    }
    else{
      printf("NO\n");
    }
    jclass class_DOMConfigurator = (*env)->FindClass(env, "org/apache/log4j/xml/DOMConfigurator");
    jmethodID mid_configure = (*env)->GetStaticMethodID( env, class_DOMConfigurator,"configure", "(Ljava/net/URL;)V");
    (*env)->CallObjectMethod(env,class_DOMConfigurator,mid_configure,url);
    
/*    
    jclass class_URL = (*env)->FindClass(env, "java/net/URL");
    jmethodID midFILE = (*env)->GetMethodID(env, class_File, "<init>", "(Ljava/lang/String;)V");
    jstring chaine = (*env)->NewStringUTF(env, "");
    jobject objet = (*env)->NewObject(env, class_File, midFILE, chaine);*/
//      DOMConfigurator.configure(GUI.class.getResource("/" + LOG_CONFIG_FILE));
//     Simulation sim = quickStartSimulationConfig(configFile, false);
    if(!mid_configure){
      printf("YES configure\n");
    }
    else{
      printf("NO\n");
    }
   
    jmethodID midFILE = (*env)->GetMethodID(env, class_File, "<init>", "(Ljava/lang/String;)V");
    jstring chaine = (*env)->NewStringUTF(env, "/home/flo/Bureau/Malisha-code/Malisha-code/automaticSimulations/5_nodes_30_minutes/autoGenTopology-5_nodes_30_minutes.csc");
    jobject objet = (*env)->NewObject(env, class_File, midFILE, chaine);

//     jclass class_Boolean = (*env)->FindClass(env, "java/lang/Boolean");
//     jmethodID midBoolean = (*env)->GetMethodID(env, class_Boolean, "<init>", "(Ljava/lang/String;)V");
//     jstring string_false = (*env)->NewStringUTF(env, "false");
//     jobject false = (*env)->NewObject(env, class_Boolean, midBoolean, string_false);
    jmethodID mid_quickStartSimulationConfig = (*env)->GetStaticMethodID( env, GUIclass,"quickStartSimulationConfig", "(Ljava/io/File;Z)Lse/sics/cooja/Simulation;");
    if(!mid_quickStartSimulationConfig){
      printf("YES mid SIM\n");
    }
    else{
      printf("NO mid SIM\n");
    }
    jobject sim = (*env)->CallStaticObjectMethod (env, GUIclass, mid_quickStartSimulationConfig, objet, JNI_FALSE);

    jmethodID StartSimulation= (*env)->GetMethodID( env,class_Simulation, "startSimulation", "()V" );
    jmethodID stopSimulation= (*env)->GetMethodID( env,class_Simulation, "stopSimulation", "(Z)V" );
    if(!StartSimulation){
      printf("YES SIM\n");
    }
    else{
      printf("NO\n");
      (*env)->CallObjectMethod(env,sim,StartSimulation,NULL);
       
// 	(*env)->CallObjectMethod(env,sim,stopSimulation,NULL);
    }
    sleep(30);
    (*env)->CallObjectMethod(env,sim,stopSimulation,JNI_FALSE);
    (*env)->DeleteLocalRef(env,sim);
    (*env)->DeleteLocalRef(env,objet);
    (*env)->DeleteLocalRef(env,setting_file);
    (*env)->DeleteLocalRef(env,file);
    if ((*env)->ExceptionOccurred(env)) {
	(*env)->ExceptionDescribe(env);
    }
    
    
    //    (*env)->SetStaticStringField (env, thisObj, fidContikiPath, "/home/flo/Bureau/Malisha-code/Malisha-code/ctk-load"");
/*	  
    jclass stringClass =(*env)->FindClass( env, "java/lang/String");
    
    if ((*env)->ExceptionOccurred(env)) {
	(*env)->ExceptionDescribe(env);
    }

    jmethodID mid = (*env)->GetStaticMethodID( env, GUIclass,"main", "([Ljava/lang/String;)V");

    jarray jArgv = (*env)->NewObjectArray( env, 2, stringClass,NULL );

//     int i;
//     for( i = 1; i < argc; i++ ) {
//       jstring jArg = (*env)->NewStringUTF( env, argv[ i ] );
//       (*env)->SetObjectArrayElement( env, jArgv, i-1, jArg );
//     }
      jstring jArg = (*env)->NewStringUTF( env, "-nogui=/home/flo/Bureau/Malisha-code/Malisha-code/automaticSimulations/5_nodes_30_minutes/autoGenTopology-5_nodes_30_minutes.csc" );
      (*env)->SetObjectArrayElement( env, jArgv, 0, jArg );
      jArg = (*env)->NewStringUTF( env, "-contiki=/home/flo/Bureau/Malisha-code/Malisha-code/ctk-load" );
      (*env)->SetObjectArrayElement( env, jArgv, 1, jArg );
      (*env)->CallStaticVoidMethod(env, GUIclass, mid, jArgv );*/
/*
      jclass classe = (*env)->FindClass(env, "se/sics/cooja/Simulation");
      jfieldID fid = (*env)->GetFieldID(
      classe, "aValue", "Lse/sics/cooja/Simulation");
      jmethodID mid = (*env)->GetMethodID(
      classe, "stopSimulation", "()V");
      (*env)->CallVoidMethod(obj, mid);*/


//        (*env)->DeleteLocalRef(env, element);
    (*jvm)->DestroyJavaVM( jvm );

    return 0;
}
// int main ()
// {
//   char str[] ="/titi/toto/cfff";
//   char * pch;
//   printf ("Splitting string \"%s\" into tokens:\n",str);
//   char * tmp=NULL ;
//   pch = strtok (str,"/");
//   while (pch != NULL)
//   {
//     printf ("%s\n",pch);
//     tmp= realloc(tmp,sizeof(char)*(strlen(pch)+1));
//     memcpy(tmp,pch,(strlen(pch)+1)*sizeof(char));
//     
//     pch = strtok (NULL, "/");
//   }
//   printf ("%s\n",tmp);
// return 0;
// }



// char** str_split(char* a_str, const char *a_delim)
// {
//     char** result    = 0;
//     size_t count     = 0;
//     char* tmp        = a_str;
//     char* last_comma = 0;
// 
//     /* Count how many elements will be extracted. */
//     while (*tmp)
//     {
//         if (*a_delim == *tmp)
//         {
//             count++;
//             last_comma = tmp;
//         }
//         tmp++;
//     }
// 
//     /* Add space for trailing token. */
//     count += last_comma < (a_str + strlen(a_str) - 1);
// 
//     /* Add space for terminating null string so caller
//        knows where the list of returned strings ends. */
//     count++;
// 
//     result = malloc(sizeof(char*) * count);
// 
//     if (result)
//     {
//         size_t idx  = 0;
//         char* token = strtok(a_str, a_delim);
// 
//         while (token)
//         {
//             assert(idx < count);
//             *(result + idx++) = strdup(token);
//             token = strtok(0, a_delim);
//         }
//         assert(idx == count - 1);
//         *(result + idx) = 0;
//     }
// 
//     return result;
// }
// 
// int main()
// {
//     char months[] = "/titi/toto/caca/home";
//     char** tokens;
// 
//     printf("months=[%s]\n\n", months);
// 
//     tokens = str_split(months, "/");
// 
//     if (tokens)
//     {
//         int i;
//         for (i = 0; *(tokens + i); i++)
//         {
//             printf("month=[%s]\n", *(tokens + i));
//             free(*(tokens + i));
//         }
//         printf("\n");
//         free(tokens);
//     }
// 
//     return 0;
// }
/*
char *str_split (char *s, const char *ct)
{
   char **tab = NULL;

   if (s != NULL && ct != NULL)
   {
      int i;
      char *cs = NULL;
      size_t size = 1;


      for (i = 0; (cs = strtok (s, ct)); i++)
      {
	 if(cs == NULL){
	    
	  }
         if (size <= i + 1)
         {
            void *tmp = NULL;
            size <<= 1;
            tmp = realloc (tab, sizeof (*tab) * size);
            if (tmp != NULL)
            {
               tab = tmp;
            }
            else
            {
               fprintf (stderr, "Memoire insuffisante\n");
               free (tab);
               tab = NULL;
               exit (EXIT_FAILURE);
            }
         }
	 
         tab[i] = cs;
         s = NULL;
	 
      }
      return tab[i-1];
      tab[i] = NULL;
   }
   return NULL;
}

int main(void){
  printf("%s\n",str_split("/toto/var/titi/caca","/"));
}*/


// int cp(const char *to, const char *from)
// {
//     int fd_to, fd_from;
//     char buf[4096];
//     ssize_t nread;
//     int saved_errno;
// 
//     fd_from = open(from, O_RDONLY);
//     if (fd_from < 0)
//         return -1;
// 
//     fd_to = open(to, O_WRONLY | O_CREAT | O_EXCL, 0666);
//     if (fd_to < 0)
//         goto out_error;
// 
//     while (nread = read(fd_from, buf, sizeof buf), nread > 0)
//     {
//         char *out_ptr = buf;
//         ssize_t nwritten;
// 
//         do {
//             nwritten = write(fd_to, out_ptr, nread);
// 
//             if (nwritten >= 0)
//             {
//                 nread -= nwritten;
//                 out_ptr += nwritten;
//             }
//             else if (errno != EINTR)
//             {
//                 goto out_error;
//             }
//         } while (nread > 0);
//     }
// 
//     if (nread == 0)
//     {
//         if (close(fd_to) < 0)
//         {
//             fd_to = -1;
//             goto out_error;
//         }
//         close(fd_from);
// 
//         /* Success! */
//         return 0;
//     }
// 
//   out_error:
//     saved_errno = errno;
// 
//     close(fd_from);
//     if (fd_to >= 0)
//         close(fd_to);
// 
//     errno = saved_errno;
//     return -1;
// }
// 
// 
// int main (void){
//   cp("/home/flo/teste.data","/home/flo/smarthomegraph.data");
//   
// }

/*
   int
   main(void)
   {
       FILE * fp;
       char * line = NULL;
       size_t len = 0;
       ssize_t read;
       int occ=0;

       fp = fopen("/home/flo/smarthomegraph.data", "r");
       if (fp == NULL)
           exit(EXIT_FAILURE);

       while ((read = getline(&line, &len, fp)) != -1) {
	    if (strstr (line, "sensor") != NULL) {
	     occ = occ + 1;
	    }
           printf("%s", line);
       }
      printf("\n%d\n",occ);
       if (line)
           free(line);
       exit(EXIT_SUCCESS);
   }*/