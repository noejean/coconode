/**
 * \file        jsonner.c
 * \author      Minh Quan HO
 * \version     1.0
 * \date        20/02/2013
 * \brief       Module JSONNER
 * \details     This module is used to export/import configuration 
 *              and generate topology
 */
#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <glib-object.h>
#include <json-glib/json-glib.h>
// #include <json-glib.h>
#include "jsonner.h"



// #define DEBUG 1

// multiples generator files:
extern GSList* files;

// Text View zone in which generator files will be shown:
extern GtkTextView* generator_files_view;

void update_generator_files_list(GtkTextView *textview, GSList* newlist){
    GtkTextIter iter,start,end;
    GtkTextBuffer *buffer;
    int i = 0;
    int len = g_slist_length(newlist); // number of files in list
    GSList *current = newlist;

    buffer = gtk_text_view_get_buffer (textview);

    gtk_text_buffer_get_start_iter (buffer,&start); 
    gtk_text_buffer_get_end_iter (buffer,&end); 
    gtk_text_buffer_delete (buffer,&start,&end); 
    gtk_text_buffer_get_start_iter(buffer, &iter);

    // loop the newlist:
    while(i < len){ 
        gchar* f = g_file_get_parse_name((GFile*)current->data);
        // write the filename in a row of list_store
        gtk_text_buffer_insert (buffer, &iter, f, -1);
        gtk_text_buffer_insert (buffer, &iter, "\n", 1);
        g_free(f);
        current = current->next;
        i++;
    }
}

/**
 * \brief       Callback of menu item "New simulation"
 * \details     Re-initialize all parameters (simulation and topology)
 * \param       widget      widget which emits signal
 * \param       data        argument data for callback
 * \return      void
 */
void cb_menu_new_simulation (GtkWidget* widget, gpointer* data)
{
    // int i = 0;
    // while(i < 10000000){
    //     printf("new simulation\n");
    //     i++;
    // }
	
}


/**
 * \brief       Callback of menu item "Export configuration"
 * \details     Open a FileChooser to choose a .json file 
 *              in which parameters will be written 
 * \param       widget      widget which emits signal
 * \param       data        argument data for callback
 * \return      void
 */
void cb_menu_export_config (GtkWidget* widget, gpointer* data)
{
	GtkWidget *dialog;
    GtkBuilder* builder;
    GtkWindow* mainwindow;

    builder = (GtkBuilder*) data;
    mainwindow = GTK_WINDOW(gtk_builder_get_object (
                        builder, "MainWindow"));
    dialog = gtk_file_chooser_dialog_new ("Save File",
     				      mainwindow,
     				      GTK_FILE_CHOOSER_ACTION_SAVE,
     				      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
     				      GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
     				      NULL);
    

    gtk_file_chooser_set_do_overwrite_confirmation (GTK_FILE_CHOOSER (dialog),
                 TRUE);
     
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER (dialog), ".");
    gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (dialog), "export-param.json");
     
    /* get parameters */
    // simulation duration in minutes:
    int hours = gtk_spin_button_get_value_as_int(
                (GtkSpinButton*) gtk_builder_get_object(builder, 
                    "simul_hours"));
    int minutes = gtk_spin_button_get_value_as_int(
                (GtkSpinButton*) gtk_builder_get_object(builder, 
                    "simul_minutes"));
    int repeat = gtk_spin_button_get_value_as_int(
                (GtkSpinButton*) gtk_builder_get_object(builder, 
                    "repeat_times"));
    gchar* protocol = gtk_file_chooser_get_filename(
                (GtkFileChooser*) gtk_builder_get_object(builder, 
                    "protocol"));
    // begin time in minutes:
    int schedule_hours = gtk_spin_button_get_value_as_int(
                (GtkSpinButton*) gtk_builder_get_object(builder, 
                    "schedule_hours"));
    int schedule_minutes = gtk_spin_button_get_value_as_int(
                (GtkSpinButton*) gtk_builder_get_object(builder, 
                    "schedule_minutes"));
    int nb_nodes = gtk_spin_button_get_value_as_int
                    ((GtkSpinButton*) gtk_builder_get_object(builder, 
                        "nbNodes"));
    int sensor_percent = gtk_spin_button_get_value_as_int
                    ((GtkSpinButton*) gtk_builder_get_object(builder, 
                        "sensorpt"));
    int actuator_percent = gtk_spin_button_get_value_as_int
                    ((GtkSpinButton*) gtk_builder_get_object(builder, 
                        "actuatorpt"));
    /*-----------------------------------------------------------------------*/

   /* Ask user to choose a file to write */
 	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
       {
        char *filename;
        filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
        /* Create a file to write */
        printf("Filename : %s\n", filename);
        FILE* file = fopen(filename, "w+");
        if (file == NULL)
        	g_print ("Fail to create file ! \n");
        else {  
        	/* write into file */
        	fprintf(file, "{\n");
            fprintf(file, "\t\"simulation\" : {\n");
            fprintf(file, "\t\t\"duration\" : {\n");
            fprintf(file, "\t\t\t\"hours\" : %d,\n ", hours);
            fprintf(file, "\t\t\t\"minutes\" : %d\n ", minutes);
            fprintf(file, "\t\t},\n");
            fprintf(file, "\t\t\"repeat\" : %d,\n", repeat);
            fprintf(file, "\t\t\"protocol\" : \"%s\",\n", protocol);
            fprintf(file, "\t\t\"beginin\" : {\n");
            fprintf(file, "\t\t\t\"schedule_hours\" : %d,\n ", schedule_hours);
            fprintf(file, "\t\t\t\"schedule_minutes\" : %d\n ",
                schedule_minutes);
            fprintf(file, "\t\t}\n");
            fprintf(file, "\t},\n");

            fprintf(file, "\t\"topology\" : {\n");
            fprintf(file, "\t\t\"nb_nodes\" : %d,\n", nb_nodes);
            fprintf(file, "\t\t\"sensor_percent\" : %d,\n", sensor_percent);
            fprintf(file, "\t\t\"actuator_percent\" : %d,\n", 
                actuator_percent);
            fprintf(file, "\t\t\"generator_files\" : [\n");

            GSList *current = files;
            int i = 1;
            int len = g_slist_length(files);
            while(i < len){
                // fprintf with ","
                fprintf(file, "\t\t\t\"%s\",\n", 
                    g_file_get_parse_name((GFile*)current->data));
                current = current->next;
                i++;
            }
            if(len > 0){
                // fprintf the last file without ","
                fprintf(file, "\t\t\t\"%s\"\n", 
                    g_file_get_parse_name((GFile*)current->data));
            }
            fprintf(file, "\t\t]\n");
            fprintf(file, "\t}\n");
            fprintf(file, "}");

        	/* end writing */
        	fclose(file); 
        }  
        g_free (filename);
       }
     
    g_free(protocol);
	gtk_widget_destroy (dialog);
}


/**
 * \brief       Callback of menu item "Import configuration"
 * \details     Open a FileChooser to choose a parameters .json 
 *                file to import 
 * \param       widget      widget which emits signal
 * \param       data        argument data for callback
 * \return      void
 */
void cb_menu_import_config (GtkWidget* widget, gpointer* data)
{
    GtkWidget *dialog;
    GtkBuilder* builder;
    GtkWindow* mainwindow;

    builder = (GtkBuilder*) data;
    mainwindow = GTK_WINDOW(gtk_builder_get_object (
                        builder, "MainWindow"));
    dialog = gtk_file_chooser_dialog_new ("Open File",
                      mainwindow,
                      GTK_FILE_CHOOSER_ACTION_OPEN,
                      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                      NULL);

   /* Ask user to choose a json file */
   if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT){
        char *filename;
        filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

        /* PARSE FILE FOR PARAMETERS */
        // init:
        g_type_init ();

        // create a parser:
        JsonParser *parser = json_parser_new ();
        GError *error = NULL;
    
        // load from JSON file and check for error:
        json_parser_load_from_file (parser, filename, &error);
        if (error){
            g_print ("Unable to parse `%s': %s\n", filename, error->message);
            g_error_free (error);
            g_object_unref (parser);
            exit(-1);
        }

        // create a reader from parser:
        JsonReader *reader = json_reader_new (json_parser_get_root (parser));

        // begin read simulation:
        json_reader_read_member (reader, "simulation");
            // ----duration:
            json_reader_read_member (reader, "duration");
                // ------hours:
                json_reader_read_member (reader, "hours");
                  int hours = json_reader_get_int_value (reader);        
                json_reader_end_member (reader);
                // ------minutes:
                json_reader_read_member (reader, "minutes");
                  int minutes = json_reader_get_int_value (reader);
                json_reader_end_member (reader);
            json_reader_end_member (reader);

            // ----repeat:
            json_reader_read_member (reader, "repeat");
               int repeat = json_reader_get_int_value (reader);
            json_reader_end_member (reader);

            // ----protocol:
            json_reader_read_member (reader, "protocol");
               const char* protocol = json_reader_get_string_value (reader);
            json_reader_end_member (reader);

            // ----beginin:
            json_reader_read_member (reader, "beginin");
                // ------schedule_hours:
                json_reader_read_member (reader, "schedule_hours");
                  int schedule_hours = json_reader_get_int_value (reader);
                json_reader_end_member (reader);
                // ------schedule_minutes:
                json_reader_read_member (reader, "schedule_minutes");
                  int schedule_minutes = json_reader_get_int_value (reader);
                json_reader_end_member (reader);
            json_reader_end_member (reader);
        json_reader_end_member (reader);
        // end read simulation

        // begin read topology:
        json_reader_read_member (reader, "topology");
            // ----nb_nodes:
            json_reader_read_member (reader, "nb_nodes");
               int nb_nodes = json_reader_get_int_value (reader);
            json_reader_end_member (reader);

            // ----sensor_percent:
            json_reader_read_member (reader, "sensor_percent");
               int sensor_percent = json_reader_get_int_value (reader);
            json_reader_end_member (reader);

            // ----actuator_percent:
            json_reader_read_member (reader, "actuator_percent");
               int actuator_percent = json_reader_get_int_value (reader);
            json_reader_end_member (reader);

            // ----generator_files:
            json_reader_read_member (reader, "generator_files");
                // create a list to store generator filenames:
                GSList* generator_files = NULL;
                int i;
                
                int array_size = json_reader_count_elements(reader);
                for (i = 0; i < array_size; i++){
                    const char* tmp;
                    json_reader_read_element (reader, i);
                    tmp = json_reader_get_string_value (reader);
                    json_reader_end_element (reader);
                    generator_files = g_slist_append(generator_files, 
                        (gpointer) g_file_parse_name(tmp));
                }
            json_reader_end_member (reader);
        json_reader_end_member (reader);
        // end read topology

  #ifdef DEBUG
   printf("Hours : %d\nMinutes : %d\nRepeat : %d\n", hours, minutes, repeat);
   printf("Protocol : %s\nSchedule_hours : %d\n", protocol, schedule_hours);
   printf("Schedule_minutes : %d\nNb_nodes : %d\n",schedule_minutes, nb_nodes);
   printf("Sensor_pctg : %d\nActuator_pctg : %d\n",
      sensor_percent, actuator_percent);
   printf("Generator files : \n");
   for(i = 0; i < g_slist_length(generator_files); i++){
     printf("\t%s \n", (char*)g_slist_nth_data(generator_files, i));
   }
  #endif


        /* FINISH PARSING JSON FOR PARAMETERS, NOW SET THEM INTO COCONODE */
        gtk_spin_button_set_value(
            ((GtkSpinButton*) gtk_builder_get_object(builder, "simul_hours")),
            hours);
        gtk_spin_button_set_value(
            (GtkSpinButton*) gtk_builder_get_object(builder, "simul_minutes"),
            minutes);
        gtk_spin_button_set_value(
            (GtkSpinButton*) gtk_builder_get_object(builder, "repeat_times"),
            repeat);
        gtk_file_chooser_set_filename(
            (GtkFileChooser*) gtk_builder_get_object(builder, "protocol"),
            protocol);
        gtk_spin_button_set_value(
            (GtkSpinButton*) gtk_builder_get_object(builder, "schedule_hours"),
            schedule_hours);
        gtk_spin_button_set_value(
            (GtkSpinButton*) gtk_builder_get_object(builder,
               "schedule_minutes"), schedule_minutes);
        gtk_spin_button_set_value(
            (GtkSpinButton*) gtk_builder_get_object(builder, "nbNodes"),
            nb_nodes);
        gtk_spin_button_set_value(
            (GtkSpinButton*) gtk_builder_get_object(builder, "sensorpt"),
            sensor_percent);
        gtk_spin_button_set_value(
            (GtkSpinButton*) gtk_builder_get_object(builder, "actuatorpt"),
            actuator_percent);

        files = generator_files;
        update_generator_files_list(generator_files_view, files);

        /* END OF SETTING PARAMETER IN SYSTEM */

        g_object_unref (reader);
        g_object_unref (parser);
        g_free (filename);
   }

    gtk_widget_destroy (dialog);
}
