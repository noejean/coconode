#include "../settings.h"
#include "gui_actions.h"

void quit(GtkWidget *widget, gpointer data)
{
	gtk_main_quit();
	printf("Done, goodbye! \n");
}

void update_combo_oneindiv(GtkWidget *combo, gpointer widgs)
{
	int active_entrie = gtk_combo_box_get_active(GTK_COMBO_BOX(combo));
	
	
	GtkTreeModel *tree_model;
	GtkTreeIter tree_iterator;
	char has_elem;
	int nb_entries;
	
	tree_model = gtk_combo_box_get_model(GTK_COMBO_BOX(combo));
	has_elem = gtk_tree_model_get_iter_first(tree_model, &tree_iterator);
	nb_entries=0;
	do
	{
		nb_entries++;
		has_elem = gtk_tree_model_iter_next(tree_model, &tree_iterator);
	} while (has_elem);
	
	printf("Combo box one_individual focused. Nb of entries: %d \n", nb_entries);
	
	int i=1;
	while ( i <= nb_entries )
	{
		gtk_combo_box_remove_text(GTK_COMBO_BOX(combo), 0);
		i++;
	}
	
	add_combo_box_entries_from_list_files(combo);
	
	gtk_combo_box_set_active(GTK_COMBO_BOX(combo), active_entrie);
}

void add_combo_box_entries_from_datain(GtkWidget *combo, const char *datain_filename)
{
	FILE *datain;
	datain = fopen(datain_filename, "r");
	
	if(datain == NULL)
	{
		fprintf(stderr, "Cannot open %s. \n", datain_filename);
		exit(EXIT_FAILURE);
	}
	
	char *s=malloc(10000*sizeof(char));
	char *s_cur = s;
	char *s_suiv = s;
	char *entries[10000];
	int size_entries;
	
	fgets(s, 9999, datain);
	//printf("%s", s);
	
	int i = 0;
	while (s_cur != NULL)
	{
		s_cur = strpbrk(s_cur, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-+=*&%$@![]{}()?<>.");
		s_suiv = strpbrk(s_cur, " 	");
		if (s_suiv != NULL)
		{
			*s_suiv = '\0';
			s_suiv=s_suiv+(1*sizeof(char));
		}
		else
			*(s_cur + (strlen(s_cur)-1)) = '\0';
		
		entries[i]=malloc(sizeof(strlen(s_cur)));
		strcpy(entries[i], s_cur);
		
		s_cur=s_suiv;
		i++;
	}
	size_entries = i++; /* i starts form 0, so i++ */
	/*
	printf("%s\n", entries[0]);
	printf("%s\n", entries[1]);
	printf("%s\n", entries[2]);
	printf("%s\n", entries[3]);
	*/
	i=0;
	while (i != size_entries)
	{
		gtk_combo_box_append_text(GTK_COMBO_BOX(combo), entries[i]);
		i++;
	}
	
	fclose(datain);
}


void add_combo_box_entries_from_list_files(GtkWidget *combo)
{
	
	char** entries = list_files(DATA_IN, FILENAME_FILTER);
	
	int index_entries = 0;
	while (*(entries + index_entries) != NULL)
	{
		gtk_combo_box_append_text(GTK_COMBO_BOX(combo), *(entries + index_entries));
		printf("%s\n", *(entries + index_entries));
		index_entries++;
	}
	
}


void generate_graph(GtkWidget *widget, gpointer widgs)
{
	
/*	((GenerateGraphWidgets*) widgs)->main_window*/
	gint main_window_width;
	gint main_window_height;
	gtk_window_get_size(GTK_WINDOW(((GenerateGraphWidgets*) widgs)->main_window), &main_window_width, &main_window_height);
	
	printf("main window resized at : %d * %d\n", main_window_width, main_window_height);
	
	
/*	((GenerateGraphWidgets*) widgs)->hbox_global_settings_status*/
	GtkRequisition hbox_global_settings_status_requisitioned_size;
	gtk_widget_size_request(((GenerateGraphWidgets*) widgs)->hbox_global_settings_status, &hbox_global_settings_status_requisitioned_size);
	printf("hbox_global_settings_status resized at : %d * %d\n", hbox_global_settings_status_requisitioned_size.width, hbox_global_settings_status_requisitioned_size.height);
	
	
/*	((GenerateGraphWidgets*) widgs)->hbox_one_individual_settings*/
	GtkRequisition hbox_one_individual_settings_requisitioned_size;
	gtk_widget_size_request(((GenerateGraphWidgets*) widgs)->hbox_one_individual_settings, &hbox_one_individual_settings_requisitioned_size);
	printf("hbox_one_individual_settings resized at : %d * %d\n", hbox_one_individual_settings_requisitioned_size.width, hbox_one_individual_settings_requisitioned_size.height);
	
	
/*	((GenerateGraphWidgets*) widgs)->hbox_one_individual_export*/
	GtkRequisition hbox_one_individual_export_requisitioned_size;
	gtk_widget_size_request(((GenerateGraphWidgets*) widgs)->hbox_one_individual_export, &hbox_one_individual_export_requisitioned_size);
	printf("hbox_one_individual_export resized at : %d * %d\n", hbox_one_individual_export_requisitioned_size.width, hbox_one_individual_export_requisitioned_size.height);
	
	
/*	((GenerateGraphWidgets*) widgs)->hbox_stat_settings*/
	GtkRequisition hbox_stat_settings_requisitioned_size;
	gtk_widget_size_request(((GenerateGraphWidgets*) widgs)->hbox_stat_settings, &hbox_stat_settings_requisitioned_size);
	printf("hbox_stat_settings resized at : %d * %d\n", hbox_stat_settings_requisitioned_size.width, hbox_stat_settings_requisitioned_size.height);
	
	
/*	((GenerateGraphWidgets*) widgs)->hbox_stat_export*/
	GtkRequisition hbox_stat_export_requisitioned_size;
	gtk_widget_size_request(((GenerateGraphWidgets*) widgs)->hbox_stat_export, &hbox_stat_export_requisitioned_size);
	printf("hbox_stat_export resized at : %d * %d\n", hbox_stat_export_requisitioned_size.width, hbox_stat_export_requisitioned_size.height);
	
	
/*	((GenerateGraphWidgets*) widgs)->graph_one_individual*/
	gint graph_one_individual_width = main_window_width/2 - 1;
	gint graph_one_individual_height = main_window_height - hbox_global_settings_status_requisitioned_size.height - hbox_one_individual_settings_requisitioned_size.height - hbox_one_individual_export_requisitioned_size.height - 2;
	printf("graph_one_individual resized at : %d * %d\n", graph_one_individual_width, graph_one_individual_height);
	
	
/*	((GenerateGraphWidgets*) widgs)->graph_stat*/
	gint graph_stat_width = main_window_width/2 - 1;
	gint graph_stat_height =  main_window_height - hbox_global_settings_status_requisitioned_size.height - hbox_stat_settings_requisitioned_size.height - hbox_stat_export_requisitioned_size.height - 2;
	printf("graph_stat resized at : %d * %d\n", graph_stat_width, graph_stat_height);
	
	
/* generate_graph */
	
/*	gchar *string_one_individual = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((GenerateGraphWidgets*) widgs)->combo_one_individual));*/
	gchar *string_stat_function = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((GenerateGraphWidgets*) widgs)->combo_stat_function));
	gchar *string_xaxis = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((GenerateGraphWidgets*) widgs)->combo_xaxis));
	gchar *string_yaxis = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((GenerateGraphWidgets*) widgs)->combo_yaxis));
	
	gint xaxis_column = gtk_combo_box_get_active(GTK_COMBO_BOX(((GenerateGraphWidgets*) widgs)->combo_xaxis));
	gint yaxis_column = gtk_combo_box_get_active(GTK_COMBO_BOX(((GenerateGraphWidgets*) widgs)->combo_yaxis));
	
	char* datain_filename_relpath = malloc(1000*sizeof(char));
/*	sprintf(datain_filename_relpath, "%s%s", DATA_IN, string_one_individual);*/
	sprintf(datain_filename_relpath, "%s%s", DATA_IN, ((GenerateGraphWidgets*) widgs)->oneindiv_traced);
	
	generate_ggraph("INDIV", datain_filename_relpath, 
		"PNG", DATA_DISPLAYED "/oneindiv.png", ((GenerateGraphWidgets*) widgs)->oneindiv_traced, 
		string_xaxis, string_yaxis, xaxis_column+1, yaxis_column+1, 
		graph_one_individual_width, graph_one_individual_height);
		
	free(datain_filename_relpath);
	
	
	
	
/*	((GenerateGraphWidgets*) widgs)->pixbuf_graph_one_individual = gdk_pixbuf_new_from_file(DATA_DISPLAYED "/oneindiv.png", NULL);*/
/*	gtk_image_set_from_pixbuf(GTK_IMAGE(((GenerateGraphWidgets*) widgs)->graph_one_individual), ((GenerateGraphWidgets*) widgs)->pixbuf_graph_one_individual);*/
	
	gtk_image_set_from_file(GTK_IMAGE(((GenerateGraphWidgets*) widgs)->graph_one_individual), DATA_DISPLAYED "/oneindiv.png");
	
	
/* end generate_graph */
	
	
	
/*	((GenerateGraphWidgets*) widgs)->pixbuf_graph_one_individual = gdk_pixbuf_new_from_file(DATA_DISPLAYED "/oneindiv.png", NULL);*/
/*	((GenerateGraphWidgets*) widgs)->pixbuf_graph_one_individual = gdk_pixbuf_scale_simple(((GenerateGraphWidgets*) widgs)->pixbuf_graph_one_individual, graph_one_individual_width, graph_one_individual_height, GDK_INTERP_BILINEAR);*/
/*	gtk_image_set_from_pixbuf(GTK_IMAGE(((GenerateGraphWidgets*) widgs)->graph_one_individual), ((GenerateGraphWidgets*) widgs)->pixbuf_graph_one_individual);*/
	
/*	((GenerateGraphWidgets*) widgs)->pixbuf_graph_stat = gdk_pixbuf_new_from_file(DATA_DISPLAYED "/stat.png", NULL);*/
/*	((GenerateGraphWidgets*) widgs)->pixbuf_graph_stat = gdk_pixbuf_scale_simple(((GenerateGraphWidgets*) widgs)->pixbuf_graph_stat, graph_stat_width, graph_stat_height, GDK_INTERP_BILINEAR);*/
/*	gtk_image_set_from_pixbuf(GTK_IMAGE(((GenerateGraphWidgets*) widgs)->graph_stat), ((GenerateGraphWidgets*) widgs)->pixbuf_graph_stat);*/
	
	if (strcmp(string_stat_function, "probability density function on mean") == 0)
	{
		char* txaxis=calloc(1000, sizeof(char));
		strcat(txaxis, string_yaxis);
		strcat(txaxis, " mean");
		vgenerate_ggraph_hist("INDIV", DATA_OUT "dataout.dat", 
		"PNG", DATA_DISPLAYED "/stat.png", string_stat_function, 
		txaxis, "frequency", 2, 1, 
		graph_stat_width, graph_stat_height);
	}
	else
	{
		generate_ggraph("STAT", DATA_OUT "dataout.dat", 
			"PNG", DATA_DISPLAYED "/stat.png", string_stat_function, 
			string_yaxis, 1, 
			graph_stat_width, graph_stat_height);
	}

	
	
	gtk_image_set_from_file(GTK_IMAGE(((GenerateGraphWidgets*) widgs)->graph_stat), DATA_DISPLAYED "/stat.png");
	
	
/*	g_free(string_one_individual);*/
	g_free(string_stat_function);
	g_free(string_xaxis);
	g_free(string_yaxis);
	
}


void trace_oneindiv_graph(GtkWidget *widget, gpointer widgs)
{
	gchar *string_one_individual = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((TraceOneIndivGraphWidgets*) widgs)->combo_one_individual));
	
/*	((TraceOneIndivGraphWidgets*) widgs)->oneindiv_traced=realloc(((TraceOneIndivGraphWidgets*) widgs)->oneindiv_traced, strlen(string_one_individual)*sizeof(char));*/
	strcpy(((TraceOneIndivGraphWidgets*) widgs)->oneindiv_traced, string_one_individual);
	printf("new individual displayed : %s, %p\n", ((TraceOneIndivGraphWidgets*) widgs)->oneindiv_traced, ((TraceOneIndivGraphWidgets*) widgs)->oneindiv_traced);
	
	printf("###trace_oneindiv_graph->(gene_graph_widgs) combo text : %s, %p\n", ((TraceOneIndivGraphWidgets*) widgs)->gene_graph_widgs->oneindiv_traced, ((TraceOneIndivGraphWidgets*) widgs)->gene_graph_widgs->oneindiv_traced);
	generate_graph(NULL, ((TraceOneIndivGraphWidgets*) widgs)->gene_graph_widgs);
	
	g_free(string_one_individual);
}


void compute_stat_graph(GtkWidget *widget, gpointer widgs)
{
	gchar *string_yaxis = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((CompStatGraphWidgets*) widgs)->combo_yaxis));
	gchar *string_stat_function = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((CompStatGraphWidgets*) widgs)->combo_stat_function));
	
	gint yaxis_column = gtk_combo_box_get_active(GTK_COMBO_BOX(((CompStatGraphWidgets*) widgs)->combo_yaxis));
	
	
	char** entries = list_files(DATA_IN, FILENAME_FILTER);
	
	char* dataout_filename = DATA_OUT "dataout.dat";
	
	FILE *dataout;
	dataout = fopen(dataout_filename, "w");
	
	if(dataout == NULL)
	{
		fprintf(stderr, "Impossible to open/create %s. \n", dataout_filename);
		exit(EXIT_FAILURE);
	}
	
	fprintf(dataout, "# Mean\n");
	
	int index_entries = 0;
	while (*(entries + index_entries) != NULL)
	{
		
		char* datain_filename_relpath = malloc(1000*sizeof(char));
		sprintf(datain_filename_relpath, "%s%s", DATA_IN, *(entries + index_entries));
		
		float mean = mean_from_datain(datain_filename_relpath, yaxis_column+1);
		
		fprintf(dataout, "%f\n", mean);
		
		free(datain_filename_relpath);
		
		index_entries++;
	}
	
	fclose(dataout);
	
	
	
	/* artefact */
	char* dataoutmean_filename = DATA_OUT "dataoutmean.dat";
	
	dataout = fopen(dataoutmean_filename, "w");
	
	if(dataout == NULL)
	{
		fprintf(stderr, "Impossible to open/create %s. \n", dataoutmean_filename);
		exit(EXIT_FAILURE);
	}
	
	fprintf(dataout, "# Mean\n");
	
	index_entries = 0;
	while (*(entries + index_entries) != NULL)
	{
		
		char* datain_filename_relpath = malloc(1000*sizeof(char));
		sprintf(datain_filename_relpath, "%s%s", DATA_IN, *(entries + index_entries));
		
		float mean = mean_from_datain(datain_filename_relpath, yaxis_column+1);
		
		fprintf(dataout, "%f\n", mean);
		
		free(datain_filename_relpath);
		
		index_entries++;
	}
	
	fclose(dataout);
	/* end artefact */
	
	
	if (strcmp(string_stat_function, "probability density function on mean") == 0)
	{
		pdff();
	}
	
	
	
	g_free(string_yaxis);
	g_free(string_stat_function);
	
	generate_graph(NULL, ((CompStatGraphWidgets*) widgs)->gene_graph_widgs);
}



void vexport_graph(GtkWidget* main_window, const char* graph_type, const char* datain_filename, 
	const char* dataout_type, const char *title, 
	const char *xlabel, const char *ylabel, int xcolumn, int ycolumn)
{
	char* default_filename_graph = malloc(100*sizeof(char));
	char is_cancelled=FALSE;
	int bitmap_width;
	int bitmap_height;
	
	
	if (strcmp(dataout_type, "EPS") == 0) /* if dataout_type == "EPS" */
		default_filename_graph="out.eps";
	else if (strcmp(dataout_type, "PDF") == 0)
		default_filename_graph="out.pdf";
	else if (strcmp(dataout_type, "PNG") == 0)
	{
		default_filename_graph="out.png";
		
		
		GtkWidget* size_dialog;
		GtkWidget* width_entry;
		GtkWidget* height_entry;
		const gchar* text_width;
		const gchar* text_height;
		
		size_dialog = gtk_dialog_new_with_buttons("Please define the size you want", 
			GTK_WINDOW(main_window), GTK_DIALOG_MODAL, GTK_STOCK_OK, GTK_RESPONSE_OK, 
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, NULL);
		
		width_entry=gtk_entry_new();
		gtk_entry_set_text(GTK_ENTRY(width_entry), DEFAULT_PNG_EXPORT_WIDTH);
		gtk_box_pack_start(GTK_BOX(GTK_DIALOG(size_dialog)->vbox), width_entry, TRUE, FALSE, 0);
		
		height_entry=gtk_entry_new();
		gtk_entry_set_text(GTK_ENTRY(height_entry), DEFAULT_PNG_EXPORT_HEIGHT);
		gtk_box_pack_start(GTK_BOX(GTK_DIALOG(size_dialog)->vbox), height_entry, TRUE, FALSE, 0);
		
		/* without this, the dialog box will be displayed, but not the entries */
		gtk_widget_show_all(GTK_DIALOG(size_dialog)->vbox);
		
		switch (gtk_dialog_run(GTK_DIALOG(size_dialog)))
		{
			case GTK_RESPONSE_OK:
				text_width = gtk_entry_get_text(GTK_ENTRY(width_entry));
				text_height = gtk_entry_get_text(GTK_ENTRY(height_entry));
				
				bitmap_width = atoi(text_width);
				bitmap_height = atoi(text_height);
				printf("png export with: width=%d, height=%d\n", bitmap_width, bitmap_height);
				break;
			case GTK_RESPONSE_CANCEL:
			case GTK_RESPONSE_NONE:
			default:
				is_cancelled=TRUE;
				break;
		}
		
		gtk_widget_destroy(size_dialog);
	}
	
	if (is_cancelled == FALSE)
	{
		GtkWidget *dialog_export_graph;
		
		dialog_export_graph = gtk_file_chooser_dialog_new("Export graph",
			GTK_WINDOW(main_window),
			GTK_FILE_CHOOSER_ACTION_SAVE,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
			NULL);
		
		gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog_export_graph), TRUE);
		
		gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog_export_graph), "./");
		gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog_export_graph), default_filename_graph);
		
		if (gtk_dialog_run(GTK_DIALOG(dialog_export_graph)) == GTK_RESPONSE_ACCEPT)
		{
			char *filename_graph;
			filename_graph = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog_export_graph));
			
			
			
			
			if (strcmp(title, "probability density function on mean") == 0)
			{
				char* txaxis=calloc(1000, sizeof(char));
				strcat(txaxis, ylabel);
				strcat(txaxis, " mean");
				vgenerate_ggraph_hist("INDIV", datain_filename, 
				dataout_type, filename_graph, title, 
				txaxis, "frequency", ycolumn+1, xcolumn+1, 
				bitmap_width, bitmap_height);
			}
			else if (strcmp(title, "mean") == 0)
			{
				vgenerate_ggraph(graph_type, datain_filename, 
					dataout_type, filename_graph, title, 
					xlabel, ylabel, 0, 1, 
					bitmap_width, bitmap_height);
			}
			else
			{
				vgenerate_ggraph(graph_type, datain_filename, 
					dataout_type, filename_graph, title, 
					xlabel, ylabel, xcolumn+1, ycolumn+1, 
					bitmap_width, bitmap_height);
			}
			
			
			
			
			
			printf("%s\n", filename_graph);
			g_free(filename_graph);
		}
		
		gtk_widget_destroy(dialog_export_graph);
	}
}



void export_graph_one_indiv(GtkWidget *widget, gpointer widgs)
{
	
	gchar *string_one_individual = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((ExportGraphOneIndivWidgets*) widgs)->combo_one_individual));
	gchar *string_xaxis = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((ExportGraphOneIndivWidgets*) widgs)->combo_xaxis));
	gchar *string_yaxis = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((ExportGraphOneIndivWidgets*) widgs)->combo_yaxis));
	
	gint xaxis_column = gtk_combo_box_get_active(GTK_COMBO_BOX(((ExportGraphOneIndivWidgets*) widgs)->combo_xaxis));
	gint yaxis_column = gtk_combo_box_get_active(GTK_COMBO_BOX(((ExportGraphOneIndivWidgets*) widgs)->combo_yaxis));
	
	
	gchar *string_format = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((ExportGraphOneIndivWidgets*) widgs)->combo_one_individual_format));
	g_print("formatt: '%s' selected\n", (string_format ? string_format : "NULL"));
/*	gint format_number = gtk_combo_box_get_active(GTK_COMBO_BOX(((ExportGraphOneIndivWidgets*) widgs)->combo_one_individual_format));*/
	
	char* datain_filename_relpath = malloc(1000*sizeof(char));
	sprintf(datain_filename_relpath, "%s%s", DATA_IN, string_one_individual);
	
	vexport_graph(((ExportGraphOneIndivWidgets*) widgs)->main_window, 
		"INDIV", datain_filename_relpath, 
		string_format, string_one_individual, 
		string_xaxis, string_yaxis, xaxis_column, yaxis_column);
	
	
	g_free(string_format);
	
	g_free(string_xaxis);
	g_free(string_yaxis);
	g_free(string_one_individual);
	
}


void export_graph_stat(GtkWidget *widget, gpointer widgs)
{
	
	gchar *string_stat_function = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((ExportGraphStatWidgets*) widgs)->combo_stat_function));
	gchar *string_xaxis = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((ExportGraphStatWidgets*) widgs)->combo_xaxis));
	gchar *string_yaxis = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((ExportGraphStatWidgets*) widgs)->combo_yaxis));
	
	gint xaxis_column = gtk_combo_box_get_active(GTK_COMBO_BOX(((ExportGraphStatWidgets*) widgs)->combo_xaxis));
	gint yaxis_column = gtk_combo_box_get_active(GTK_COMBO_BOX(((ExportGraphStatWidgets*) widgs)->combo_yaxis));
	
	
	gchar *string_format = gtk_combo_box_get_active_text(GTK_COMBO_BOX(((ExportGraphStatWidgets*) widgs)->combo_stat_format));
	g_print("formatt: '%s' selected\n", (string_format ? string_format : "NULL"));
/*	gint format_number = gtk_combo_box_get_active(GTK_COMBO_BOX(((ExportGraphStatWidgets*) widgs)->combo_stat_format));*/
	
	
	vexport_graph(((ExportGraphStatWidgets*) widgs)->main_window, 
		"STAT", DATA_OUT "dataout.dat", 
		string_format, string_stat_function, 
		NULL, string_yaxis, xaxis_column, yaxis_column);
	
	
	g_free(string_format);
	
	g_free(string_xaxis);
	g_free(string_yaxis);
	g_free(string_stat_function);
	
}



